<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\crypto\library;

use liberty_code\library\instance\model\Multiton;

use Closure;
use Iterator;
use ReflectionFunction;
use liberty_code\library\datetime\library\ToolBoxDateTime;



class ToolBoxHash extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get string hash digest,
     * from specified iterator values.
     *
     * @param array|Iterator $iterator
     * @param boolean $boolHashIteratorValueRequired = false
     * @return null|string
     */
    protected static function getStrHashDigestFromIteratorValue($iterator, $boolHashIteratorValueRequired = false)
    {
        // Init var
        $boolHashIteratorValueRequired = (is_bool($boolHashIteratorValueRequired) ? $boolHashIteratorValueRequired : false);
        $result = null;

        // Get hash, if required
        if(is_array($iterator) || ($iterator instanceof Iterator))
        {
            // Run all values
            $result = '';
            foreach($iterator as $key => $value)
            {
                // Get value hash
                $strValueHash = static::getStrHash($value, $boolHashIteratorValueRequired);

                // Register value hash, if required
                if(is_null($strValueHash)) {
                    $result = null;
                    break;
                }
                $result .= (strval($key) . $strValueHash);
            }
        }

        // Return result
        return $result;
    }



	/**
     * Get string hash,
     * from specified value.
	 * 
	 * @param mixed $value
     * @param boolean $boolHashIteratorValueRequired = false
	 * @return string
     */
    public static function getStrHash($value, $boolHashIteratorValueRequired = false)
    {
		// Init var
        $boolHashIteratorValueRequired = (is_bool($boolHashIteratorValueRequired) ? $boolHashIteratorValueRequired : false);

        // Get hash digest, in case of closure
        if($value instanceof Closure)
        {
            // Get hash digest, from closure definition
            $strHashDigest = (new ReflectionFunction($value))->__toString();
        }
		// Get hash digest, in case of object
		else if(is_object($value))
		{
            $strHashDigest = @spl_object_hash($value);

            // Add iterator value hashes, if required
            $strHashDigest = (
                (
                    $boolHashIteratorValueRequired &&
                    ($value instanceof Iterator)
                ) ?
                    ($strHashDigest . static::getStrHashDigestFromIteratorValue($value, $boolHashIteratorValueRequired)) :
                    $strHashDigest
            );
		}
		// Get hash digest, in case of array
		else if(is_array($value))
		{
            $strHashDigest = static::getStrHashDigestFromIteratorValue($value, $boolHashIteratorValueRequired);
            $strHashDigest = ((trim($strHashDigest) === '') ? @serialize($value) : $strHashDigest);
		}
		// Get hash digest, in case else
		else
		{
            $strHashDigest = @serialize($value);
		}
		
		// Get hash
        $result = md5($strHashDigest);
		
        // Return result
        return $result;
    }
	
	
	
	/**
     * Get string unique hash (based on date-time now object and micro-time now).
	 * 
	 * @return string
     */
    public static function getStrHashUniq()
    {
		// Init var
		$objDt = ToolBoxDateTime::getObjDtNow(); // Based on configured time zone
		$strTime = $objDt->getTimestamp();
		$strMicroTime = microtime();
        $strUniq = $strTime . $strMicroTime;
		$result = static::getStrHash($strUniq);
		
        // Return result
        return $result;
    }
	
	
	
}