<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
include($strRootAppPath . '/src/reflection/test/ClassTest1.php');

// Use
use liberty_code\library\crypto\library\ToolBoxHash;
use liberty_code\library\reflection\test\ClassTest1;



// Test hash
$intArg1 = 7;
$strArg2 = 'Test class';
$boolArg3 = true;
$obj1 = new ClassTest1($intArg1, $strArg2, $boolArg3);
$obj2 = $obj1;
$call1 = function($strNm) {
    return strtoupper($strNm);
};
$tabValue = array(
	'test_1',
	2,
    2,
	2.5,
	false,
	true,
	[
		'test_1',
		1.1,
		[
			1, 
			'test 1_1', 
			true
		]
	],
    [
        'test_1',
        1.1,
        [
            1,
            'test 1_1',
            true
        ]
    ],
	new ClassTest1($intArg1, $strArg2, $boolArg3),
	new ClassTest1($intArg1, $strArg2, $boolArg3),
	[
		$test = new ClassTest1($intArg1, $strArg2, $boolArg3),
		2, 
		'test 2', 
		false
	],
    [
        //$test,
        new ClassTest1($intArg1, $strArg2, $boolArg3),
        2,
        'test 2',
        false
    ],
	'',
	null,
	array(),
    array(),
    $obj1,
    $obj2,
    $call1,
    $call1
);

foreach($tabValue as $value)
{
	// Get info
	$strHash = ToolBoxHash::getStrHash($value);
	
	echo('Test hash: <br />');
	echo('Value: <pre>');var_dump($value);echo('</pre>');
	echo('Hash: <pre>');var_dump($strHash);echo('</pre>');
	
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test unique hash
echo('Test unique hash: <br />');

for($intCpt = 0; $intCpt < 10; $intCpt++)
{
	$strHash = ToolBoxHash::getStrHashUniq();
	echo('Hash "' . strval($intCpt + 1) . '": <pre>');var_dump($strHash);echo('</pre>');
	echo('<br />');
}

echo('<br /><br /><br />');


