<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\table\exception;

use Exception;

use liberty_code\library\table\library\ConstTable;



class MergeOptionInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $option
     */
	public function __construct($option)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstTable::EXCEPT_MSG_MERGE_OPTION_INVALID_FORMAT, strval($option));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified option has valid format.
	 * 
     * @param mixed $option
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($option)
    {
		// Init var
		$tabOption = ConstTable::getTabMergeOption();
		$result = (is_string($option) && in_array($option, $tabOption));
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($option);
		}
		
		// Return result
		return $result;
    }
	
	
	
}