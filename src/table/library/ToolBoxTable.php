<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\table\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\library\table\library\ConstTable;
use liberty_code\library\table\exception\MergeOptionInvalidFormatException;
use liberty_code\library\table\exception\MergeOptionStopException;



class ToolBoxTable extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	


	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified array is indexed.
     *
     * @param array $tabItem
     * @param boolean $boolIsOrdered = false
     * @return boolean
     */
    public static function checkTabIsIndex(array $tabItem, $boolIsOrdered = false)
    {
        // Init var
        $result = true;
        $boolIsOrdered = (is_bool($boolIsOrdered) ? $boolIsOrdered : false);

        // Run each key
        $tabKey = array_keys($tabItem);
        for($intCpt = 0; ($intCpt < count($tabKey)) && $result; $intCpt++)
        {
            $key = $tabKey[$intCpt];

            // Check key
            $result =
                // Check is valid integer
                (
                    is_int($key) ||
                    (is_string($key) && ctype_digit($key))
                ) &&
                (intval($key) >= 0) &&

                // Check is valid order, if required
                (
                    (!$boolIsOrdered) ||
                    (intval($key) === $intCpt)
                );
        }

        // Return result
        return $result;
    }





    // Methods merge getters
    // ******************************************************************************

    /**
     * Get merged array,
     * from specified source array (array 1),
     * and specified additional array (array 2),
     * with children resolution (recursive).
     *
     * @param array $tabData1
     * @param array $tabData2
     * @param string $strMergeOption = ConstTable::OPTION_MERGE_OVERWRITE
     * @param boolean $boolIndexAddRequire = true
     * @return array
     * @throws MergeOptionInvalidFormatException
     * @throws MergeOptionStopException
     */
    public static function getTabMerge(
        array $tabData1,
        array $tabData2,
        $strMergeOption = ConstTable::OPTION_MERGE_OVERWRITE,
        $boolIndexAddRequire = true
    )
    {
        // Check merge option
        MergeOptionInvalidFormatException::setCheck($strMergeOption);

        // Init var
        $result = $tabData1;
        $boolIndexAddRequire = (is_bool($boolIndexAddRequire) ? $boolIndexAddRequire : true);

        // Run all elements on array 2
        foreach($tabData2 as $key => $value)
        {
            // Check index add required
            if(
                (is_int($key) || ctype_digit($key)) &&
                $boolIndexAddRequire
            )
            {
                $result[] = $value;
            }
            // Check add required
            else if (!isset($result[$key]))
            {
                $result[$key] = $value;
            }
            // Case else (key already found): Check merge required
            else
            {
                // Case value is array: Merge value array
                if(is_array($value))
                {
                    $result[$key] = (is_array($result[$key]) ? $result[$key] : array($result[$key]));
                    $result[$key] = static::getTabMerge(
                        $result[$key],
                        $value,
                        $strMergeOption,
                        $boolIndexAddRequire
                    );
                }
                // Case else: Merge value
                else
                {
                    // Process by merge option
                    switch ($strMergeOption) {
                        case ConstTable::OPTION_MERGE_OVERWRITE:
                            $result[$key] = $value;
                            break;

                        case ConstTable::OPTION_MERGE_IGNORE:
                            // Do nothing
                            break;

                        case ConstTable::OPTION_MERGE_STOP:
                            throw new MergeOptionStopException($key);
                            break;
                    }
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get merged array,
     * from specified arrays,
     * with children resolution (recursive).
     *
     * @param array $tabMerge
     * @param string $strMergeOption = ConstTable::OPTION_MERGE_OVERWRITE
     * @param boolean $boolIndexAddRequire = true
     * @return array
     */
    public static function getTabMergeTabMulti(
        array $tabMerge,
        $strMergeOption = ConstTable::OPTION_MERGE_OVERWRITE,
        $boolIndexAddRequire = true
    )
    {
        // Init var
        $result = (
            (isset($tabMerge[0]) && is_array($tabMerge[0])) ?
                $tabMerge[0] :
                array()
        );

        // Run each arrays
        for($intCpt = 1; $intCpt < count($tabMerge); $intCpt++)
        {
            // Get merged array
            $result = (
                is_array($tabMerge[$intCpt]) ?
                    static::getTabMerge(
                        $result,
                        $tabMerge[$intCpt],
                        $strMergeOption,
                        $boolIndexAddRequire
                    ) :
                    $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get merged array,
     * with children resolution (recursive).
     *
     * @param ...array
     * @return array
     */
    public static function getTabMergeMulti()
    {
        // Return result
        return static::getTabMergeTabMulti(func_get_args());
    }





    // Methods item check
    // ******************************************************************************

    /**
     * Check if specified item exists,
     * on specified array of items.
     *
     * Key format:
     * @see getSearchItem() key format.
     *
     * @param array $tabItem
     * @param integer|string|array $key
     * @return boolean
     */
    public static function checkItemExists(array $tabItem, $key)
    {
        // Init var
        $result = true;
        static::getSearchItem(
            $tabItem,
            $key,
            null,
            $result
        );

        // Return result
        return $result;
    }





    // Methods item getters
    // ******************************************************************************

    /**
     * Search specified item,
     * from specified array of items.
     *
     * Key format:
     * Integer|string key:
     * Use key to find item, from array of items.
     * OR
     * [index array of integer|string keys]:
     * Use each key, in same order (like a path), to find sub-item, from array of items.
     *
     * @param array &$tabItem
     * @param integer|string|array $key
     * @param null|mixed $default = null
     * @param boolean &$boolFound = false
     * @param boolean $boolCreateRequired = false
     * @param null|string $strLastIndexKey = null
     * @return mixed|array
     */
    public static function &getSearchItem(
        array &$tabItem,
        $key,
        $default = null,
        &$boolFound = false,
        $boolCreateRequired = false,
        $strLastIndexKey = null
    )
    {
        // Init var
        $tabKey = (
            (is_string($key) || is_numeric($key)) ?
                array($key) :
                (
                    is_array($key) ?
                        array_filter(
                            $key,
                            function($key) {return (is_string($key) || is_numeric($key));}
                        ) :
                        array()
                )
        );
        $key = array_shift($tabKey);
        $boolFound = false;
        $boolCreateRequired = (is_bool($boolCreateRequired) ? $boolCreateRequired : false);
        $strLastIndexKey = (is_string($strLastIndexKey) ? $strLastIndexKey : null);
        $result = $default;

        // Add item, if required
        if($boolCreateRequired)
        {
            // Add item, as last index, if required
            if(
                (!is_null($strLastIndexKey)) &&
                is_string($key) &&
                ($strLastIndexKey == $key)
            )
            {
                $tabItem[] = (
                    (count($tabKey) == 0) ?
                        $default :
                        array()
                );
                $key = (count($tabItem) - 1);
            }
            // Add item, as key, else
            else
            {
                $tabItem[$key] = (
                    (count($tabKey) == 0) ?
                        (
                            array_key_exists($key, $tabItem) ?
                                $tabItem[$key] :
                                $default
                        ) :
                        (
                            array_key_exists($key, $tabItem) ?
                                (
                                    is_array($tabItem[$key]) ?
                                        $tabItem[$key] :
                                        array($tabItem[$key])
                                ) :
                                array()
                        )
                );
            }
        }

        // Get item, if found
        if(array_key_exists($key, $tabItem))
        {
            // Get sub-item, if required
            if(count($tabKey) > 0)
            {
                if(is_array($tabItem[$key]))
                {
                    $result = &static::getSearchItem(
                        $tabItem[$key],
                        $tabKey,
                        $default,
                        $boolFound,
                        $boolCreateRequired,
                        $strLastIndexKey
                    );
                }
            }
            // Get item else
            else
            {
                $boolFound = true;
                $result = &$tabItem[$key];
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get specified item(s),
     * from specified array of items.
     *
     * Key format:
     * Null: Get array of items.
     * OR
     * @see getSearchItem() key format.
     *
	 * @param array $tabItem
	 * @param null|integer|string|array $key = null
	 * @param null|mixed $default = null
     * @return array|mixed
     */
    public static function getItem(
        array $tabItem,
        $key = null,
        $default = null
    )
	{
        // Init var
        $result = (
            is_null($key) ?
                // Get array of items, if required
                $tabItem :
                // Get specified item, else
                static::getSearchItem(
                    $tabItem,
                    $key,
                    $default
                )
        );
		
		// Return result
		return $result;
	}





    // Methods item setters
    // ******************************************************************************

    /**
     * Set specified item,
     * on specified array of items.
     *
     * Key format:
     * @see getSearchItem() key format.
     *
     * @param array &$tabItem
     * @param integer|string|array $key
     * @param mixed $item
     * @param boolean $boolCreateRequired = false
     * @param null|string $strLastIndexKey = null
     * @return boolean
     */
    public static function setItem(
        array &$tabItem,
        $key,
        $item,
        $boolCreateRequired = false,
        $strLastIndexKey = null
    )
    {
        // Init var
        $result = false;
        $searchItem = &static::getSearchItem(
            $tabItem,
            $key,
            null,
            $result,
            $boolCreateRequired,
            $strLastIndexKey
        );

        // Set item, if required
        if($result)
        {
            $searchItem = $item;
        }

        // Return result
        return $result;
    }



    /**
     * Remove specified item,
     * from specified array of items.
     *
     * Key format:
     * @see getSearchItem() key format.
     *
     * @param array &$tabItem
     * @param integer|string|array $key
     * @return boolean
     */
    public static function removeItem(
        array &$tabItem,
        $key
    )
    {
        // Init var
        $tabKey = (is_array($key) ? $key : array($key));
        $key = array_pop($tabKey);
        $result = true;

        // Init parent item
        $tabParentItem = &$tabItem;
        if(count($tabKey) > 0)
        {
            $tabParentItem = &static::getSearchItem(
                $tabItem,
                $tabKey,
                null,
                $result
            );
        }

        // Remove item, if required
        $result = (
            $result &&
            (is_string($key) || is_numeric($key)) &&
            array_key_exists($key, $tabParentItem)
        );
        if($result)
        {
            unset($tabParentItem[$key]);
        }

        // Return result
        return $result;
    }



}