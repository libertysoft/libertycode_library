<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\table\library;



class ConstTable
{
    // ******************************************************************************
    // Constants
    // ******************************************************************************

    // Options for data builder merge conflict
    const OPTION_MERGE_OVERWRITE = 'overwrite'; // Keep last
    const OPTION_MERGE_IGNORE = 'ignore'; // Keep first
    const OPTION_MERGE_STOP = 'stop'; // Stop script (throw exception)
	
	
	
	// Exception message constants
    const EXCEPT_MSG_MERGE_OPTION_INVALID_FORMAT = 'Following merge type "%1$s" invalid! The merge type must be a valid option.';
    const EXCEPT_MSG_MERGE_OPTION_STOP = 'Impossible to resolve conflict for the following key "%1$s".';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods statics getters
    // ******************************************************************************

    /**
     * Get index array of merge options.
     *
     * @return array
     */
    public static function getTabMergeOption()
    {
        // Return result
        return array(
            self::OPTION_MERGE_OVERWRITE,
            self::OPTION_MERGE_IGNORE,
            self::OPTION_MERGE_STOP
        );
    }


	
} 