<?php

// Start session
session_start();

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\library\table\library\ConstTable;
use liberty_code\library\table\library\ToolBoxTable;



// Init var
$tabData1 = array(
    'root' => [
        't1' => [
            't1_1' => 'Value 1_1',
            't1_2' => 'Value 1_2'
        ],
        't2' => 2,
        't3' => [
            't3_1' => 'Value 3_1',
            't3_2' => [3, 4]
        ]
    ]
);

$tabData2 = array(
    'root' => [
        't1' => [
            't1_1' => 'Value 1_1 update', // Update
            't1_2' => 'Value 1_2',
            't1_3' => 'Value 1_3' // Add
        ],
        't3' => [
            't3_2' => [7], // Update
            't3_3' => true // Add
        ],
        't4' => 'Value 4' // Add
    ]
);

$tabData3 = array(
    'root' => [
        't2' => 2,
        't4' => 'Value 4 update', // Update
        't5' => 5, // Add
        't6' => true // Add
    ]
);

$tabData4 = array(
    'root' => [
        't1' => [
            't1_1' => 'Value 1_1',
            't1_2' => 'Value 1_2',
            't1_3' => 'Value 1_3'
        ],
        't2' => [
            'Value 2_1',
            'Value 2_2',
            'Value 2_3'
        ]
    ]
);

$tabData5 = array(
    'root' => [
        't1' => [
            't1_3' => 'Value 1_3 upd',
            't1_4' => 'Value 1_4',
            't1_5' => 'Value 1_5'
        ],
        't2' => [
            'Value 2_3 upd',
            'Value 2_4',
            'Value 2_5'
        ]
    ]
);

$tabData6 = array(
    'root' => [
        't1' => [
            't1_5' => 'Value 1_5 upd',
            't1_6' => 'Value 1_6',
            't1_7' => 'Value 1_7'
        ],
        't2' => [
            'Value 2_5 upd',
            'Value 2_6',
            'Value 2_7'
        ]
    ]
);



// Merge 2 tables
echo('Merge 2 tables : <br />');
echo('<pre>');print_r(ToolBoxTable::getTabMerge(
    $tabData1,
    $tabData2,
    ConstTable::OPTION_MERGE_OVERWRITE,
    false
));echo('</pre>'); // ConstTable::OPTION_MERGE_OVERWRITE, ConstTable::OPTION_MERGE_IGNORE, ConstTable::OPTION_MERGE_STOP
echo('<br />');

echo('<br /><br /><br />');



// Merge multi tables
echo('Merge multi tables : <br />');
echo('<pre>');print_r(ToolBoxTable::getTabMergeMulti($tabData1, $tabData2, $tabData3));echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');



// Merge multi tables 2
echo('Merge multi tables 2: <br />');
echo('<pre>');print_r(ToolBoxTable::getTabMergeMulti($tabData4, $tabData5, $tabData6));echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');



// Check table is indexed
$tabTabItem = array(
    // Not indexed
    [
        'key_1' => 'value 1',
        'key_2' => 'value 2',
        'key_3' => 'value 3',
    ],

    // Indexed, ordered
    [
        'value 1',
        'value 2',
        'value 3',
    ],

    // Not indexed
    [
        'value 1',
        'key_2' => 'value 2',
        'value 3',
    ],

    // Indexed, ordered
    [
        0 => 'value 1',
        '1' => 'value 2',
        2 => 'value 3',
    ],

    // Not indexed
    [
        0 => 'value 1',
        'key_2' => 'value 2',
        2 => 'value 3',
    ],

    // Indexed, not ordered
    [
        'value 1',
        1 => 'value 2',
        3 => 'value 3',
    ],

    // Not indexed
    [
        'value 1',
        1 => 'value 2',
        -2 => 'value 3',
    ],

    // Indexed, not ordered
    [
        'value 1',
        '2' => 'value 2',
        '1' => 'value 3',
    ]
);
echo('Check table is indexed : <br />');

foreach($tabTabItem as $tabItem)
{
    echo('Table: <pre>');print_r($tabItem);echo('</pre>');
    echo('Indexed: <pre>');var_dump(ToolBoxTable::checkTabIsIndex($tabItem));echo('</pre>');
    echo('Indexed (and ordered): <pre>');var_dump(ToolBoxTable::checkTabIsIndex($tabItem, true));echo('</pre>');
    echo('<br />');
}

echo('<br /><br /><br />');



// Check and get item
$tabItem = array(
    'key-1' => [
        'Value 1',
        1,
        true
    ],
    'key-2' => [
        'key-2-1' => 'Value 2.1',
        'key-2-2' => 2,
        'key-2-3' => [
            'key-2-3-1' => 'Value 2.3.1',
            2.2,
            false
        ]
    ],
    'key-3' => 3,
    'key-4' => null
);

$tabKey = array(
    'key-1', // Ok
    'key1', // Ko: Not found
    ['key-1'], // Ok
    ['key-1', '0'], // Ok
    ['key-1', 0], // Ok
    ['key-1', 15], // Ko: Not found
    ['key-2', 'key-2-3', 0], // Ok
    ['key-2', 'key-2-3', 3], // Ko: Not found
    ['key-2', 'key-2-3', 'key-2-3-1'], // Ok
    ['key-4'] // Ok
);
echo('Check and get item: <br />');

foreach($tabKey as $key)
{
    echo('Key: <pre>');var_dump($key);echo('</pre>');
    echo('Check item exists: <pre>');var_dump(ToolBoxTable::checkItemExists($tabItem, $key));echo('</pre>');
    echo('Get item: <pre>');var_dump(ToolBoxTable::getItem($tabItem, $key, null));echo('</pre>');
    echo('<br />');
}

echo('Get array of items: <pre>');print_r($tabItem);echo('</pre>');

echo('<br /><br /><br />');



// Set item
$tabKey = array(
    'key-1', // Ok
    'key1', // Ko: Not found
    ['key-1', 0], // Ok
    ['key-1', 15], // Ko: Not found
    ['key-2', 'key-2-3', 'key-2-3-1'], // Ok
    ['key-2', 'key-2-3', 'key-2-3-1', '[]'], // Ko: Not found
    ['key-2', 'key-2-3', 3], // Ko: Not found
    ['key-4'] // Ok
);
echo('Set item: <br />');

foreach($tabKey as $key)
{
    $tabItemSet = $tabItem;

    echo('Key: <pre>');var_dump($key);echo('</pre>');
    echo('Set item: <pre>');
    var_dump(ToolBoxTable::setItem($tabItemSet, $key, 'Test new Value'));
    echo('</pre>');
    echo('Get array of items: <pre>');var_dump($tabItemSet);echo('</pre>');
    echo('<br />');
}

echo('<br /><br /><br />');



// Set|add item
$tabKey = array(
    'key-1', // Ok
    'key1', // Ok: Create
    ['key-1', 0], // Ok
    ['key-1', '[]'], // Ok: Create
    ['key-2', 'key-2-3', 'key-2-3-1'], // Ok
    ['key-2', 'key-2-3', 'key-2-3-1', '[]'], // Ok: Create
    ['key-2', 'key-2-3', 7], // Ok: Create
    ['key-4'] // Ok
);
echo('Set|add item: <br />');

foreach($tabKey as $key)
{
    $tabItemSet = $tabItem;

    echo('Key: <pre>');var_dump($key);echo('</pre>');
    echo('Set|add item: <pre>');
    var_dump(ToolBoxTable::setItem($tabItemSet, $key, 'Test new Value', true, '[]'));
    echo('</pre>');
    echo('Get array of items: <pre>');var_dump($tabItemSet);echo('</pre>');
    echo('<br />');
}

echo('<br /><br /><br />');



// Remove item
$tabKey = array(
    'key-1', // Ok
    'key1', // Ko
    ['key-1', 0], // Ok
    ['key-1', '[]'], // Ko
    ['key-2', 'key-2-3', 'key-2-3-1'], // Ok
    ['key-2', 'key-2-3', 7], // Ko
    ['key-4'] // Ok
);
echo('Remove item: <br />');

foreach($tabKey as $key)
{
    $tabItemRemove = $tabItem;

    echo('Key: <pre>');var_dump($key);echo('</pre>');
    echo('Remove item: <pre>');
    var_dump(ToolBoxTable::removeItem($tabItemRemove, $key));
    echo('</pre>');
    echo('Get array of items: <pre>');var_dump($tabItemRemove);echo('</pre>');
    echo('<br />');
}

echo('<br /><br /><br />');


