<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\instance\exception;

use Exception;

use liberty_code\library\instance\library\ConstInstance;



class OptionInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $option
     */
	public function __construct($option) 
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstInstance::EXCEPT_MSG_OPTION_INVALID_FORMAT, strval($option));
	}





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified option has valid format.
     *
     * @param mixed $option
     * @return boolean
     * @throws static
     */
    public static function setCheck($option)
    {
        // Init var
        $result = (
            is_string($option) &&
            in_array(
                $option,
                array(
                    ConstInstance::OPTION_DEFAULT_FIRST,
                    ConstInstance::OPTION_DEFAULT_FIRST
                )
            )
        );

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static($option);
        }

        // Return result
        return $result;
    }



}