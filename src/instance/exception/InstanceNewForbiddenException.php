<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\instance\exception;

use Exception;

use liberty_code\library\instance\library\ConstInstance;



class InstanceNewForbiddenException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     */
	public function __construct() 
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = ConstInstance::EXCEPT_MSG_INSTANCE_NEW_FORBIDDEN;
	}
	
	
	
}