<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\instance\exception;

use Exception;

use liberty_code\library\instance\library\ConstInstance;



class IndexNotFoundException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $index
     */
	public function __construct($index) 
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstInstance::EXCEPT_MSG_INDEX_NOT_FOUND, strval($index));
	}
	
	
	
}