<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\instance\library;



class ConstInstance
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************
	
	// Configuration
	const PARAM_MULTITON_LIMIT = 0;
	
	const OPTION_DEFAULT_FIRST = 'opt_default_first';
	const OPTION_DEFAULT_LAST = 'opt_default_last';
	
	
	
	// Exception message constants
	const EXCEPT_MSG_INDEX_INVALID_FORMAT = 'Following index "%1s" invalid! The index must be a positive integer.';
	const EXCEPT_MSG_INDEX_NOT_FOUND = 'Following index "%1s" out of bound!';
	const EXCEPT_MSG_INSTANCE_NOT_FOUND = 'Instance unfound in instances table!';
	const EXCEPT_MSG_INSTANCE_NEW_FORBIDDEN = 'Impossible to create new instance! This action is forbidden.';
	const EXCEPT_MSG_COUNT_INVALID_FORMAT = 'Following count "%1s" invalid! The count must be a positive integer.';
	const EXCEPT_MSG_OPTION_INVALID_FORMAT = 'Following option "%1s" invalid!';



} 