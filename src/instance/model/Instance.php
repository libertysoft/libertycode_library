<?php
/**
 * Description :
 * This class allows to define instance class.
 * Instance allows to handle its owns class instances.
 * Can be consider is base of all instance types.
 *
 * Note:
 * -> Instances storage:
 *     - All instances, from current extended class, are stored in instances array.
 *     - Automatically set/remove instances, during instance creation/destruction standard flow.
 *     - Redeclare 'static protected $__instanceTab = array();' in extended class, to manage its own instances,
 *     independently from its parent.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\instance\model;

use ReflectionClass;
use liberty_code\library\instance\exception\IndexInvalidFormatException;
use liberty_code\library\instance\exception\IndexNotFoundException;
use liberty_code\library\instance\exception\InstanceNotFoundException;



abstract class Instance
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Index array of instances.
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     */
	public function __construct() 
	{
		// Set instance
		$this->instanceSet();
	}
	
	
	
	/**
	 * Clone
     */
	public function __clone()
    {
		// Set instance
        $this->instanceSet();
    }
	
	
	
	/**
	 * Destructor
     */
	public function __destruct() 
	{
        // Remove instance
        $this->instanceRemove();
	}
	
	
	
	
	
	// Methods check
	// ******************************************************************************
	
	/**
	 * Check if specified instance exists.
	 * 
     * @param mixed $objInstance
	 * @return boolean
     */
	public static function instanceCheckExists($objInstance)
    {
		// Return result
		return (in_array($objInstance, static::$__instanceTab, true));
    }
	
	
	
	/**
	 * Check if specified instance index exists.
	 * 
     * @param integer $intIndex
	 * @return boolean
	 * @throw IndexInvalidFormatException
     */
	public static function  instanceCheckIndexExists($intIndex)
    {
		// Set check format index
		IndexInvalidFormatException::setCheck($intIndex);

		// Return result
		return isset(static::$__instanceTab[$intIndex]);
    }
	
	
	
	
	
	// Methods getters
	// ******************************************************************************
	
	/**
	 * Get count of all instances.
	 * 
	 * @return integer
     */
	public static function instanceGetIntCount()
    {
		// Return result
		return count(static::$__instanceTab);
    }
	
	
	
	/**
	 * Get instance index,
     * from specified instance.
	 * 
     * @param mixed $objInstance
	 * @return integer
	 * @throws InstanceNotFoundException
     */
	public static function instanceGetIntIndex($objInstance)
    {
		// Init var
		$result = array_search($objInstance, static::$__instanceTab, true);;
		
		// Check instance index found
		if(!is_int($result))
		{
			throw new InstanceNotFoundException();
		}
		
		// Return result
		return $result;
    }
	
	
	
	/**
	 * Get instance,
     * from specified instance index.
	 * 
     * @param integer $intIndex = 0
	 * @return static
	 * @throws IndexNotFoundException
     */
	public static function instanceGet($intIndex = 0)
    {
		// Init var
		$result = static::$__instanceTab[$intIndex] ?? null;
		
		// Check instance found
		if(is_null($result))
		{
			throw new IndexNotFoundException($intIndex);
		}
		
		// Return result
		return $result;
    }
	
	
	
	/**
	 * Get first instance.
	 * 
	 * @return static
     */
	public static function instanceGetFirst()
    {
		// Return result
		return static::instanceGet();
    }
	
	
	
	/**
	 * Get last instance.
	 * 
	 * @return static
     */
	public static function instanceGetLast()
    {
		// Return result
		return static::instanceGet((static::instanceGetIntCount() - 1));
    }
	
	
	
	/**
	 * Get new instance.
	 * 
	 * @return static
     */
	public static function instanceGetNew()
    {
		// Init var
        /** @var static $result */
		$result = (new ReflectionClass(get_called_class()))->newInstanceArgs(func_get_args());
		
		// Return result
		return $result;
    }
	
	
	
	
	
	// Methods setters
	// ******************************************************************************

    /**
     * Set instance.
     */
    protected function instanceSet()
    {
        // Set instance
        static::$__instanceTab[] = $this;
    }



    /**
     * Remove instance.
     */
    protected function instanceRemove()
    {
        // Init var
        $intIndex = static::instanceGetIntIndex($this);

        // Remove instance
        unset(static::$__instanceTab[$intIndex]);
        static::$__instanceTab = array_values(static::$__instanceTab); // Reindex
    }


	
}