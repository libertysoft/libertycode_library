<?php
/**
 * Description :
 * This class allows to define multiton class.
 * Multiton is instance, allows to handle class instantiation control.
 * Can be consider is base of all multiton types.
 *
 * Note:
 * -> Set instance creation limitation ($__instanceIntCountLimit):
 *     - Must be positive integer (>= 0). By default = ConstInstance::PARAM_MULTITON_LIMIT.
 *     - O means no limitation, 1 means only one instance allowed (singleton), ... N means N instances allowed (Ngleton).
 * -> Set default instance option ($__instanceStrOptionDefault):
 *     - Allows to define default instance selection.
 *     - Must be 1 of following values:
 *         - ConstInstance::OPTION_DEFAULT_LAST: means last instance is considered as default instance.
 *         - ConstInstance::OPTION_DEFAULT_FIRST: means first instance is considered as default instance.
 * -> Example: singleton:
 *     - class Singleton extends Multiton{
 *         static protected $__instanceTab = array();
 *         static protected $__instanceIntCountLimit = 1;
 *	       static protected $__instanceStrOptionDefault = ConstInstance::OPTION_DEFAULT_LAST;
 *	       ...
 *     }
 *     - Singleton::instanceGetDefault(...constructor parameters)
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\instance\model;

use liberty_code\library\instance\library\ConstInstance;
use liberty_code\library\instance\model\Instance;
use liberty_code\library\instance\exception\InstanceNewForbiddenException;
use liberty_code\library\instance\exception\OptionInvalidFormatException;



abstract class Multiton extends Instance
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances array, to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	/**
	 * Instances limit max.
	 * If <= 0 : no limit
     * @var int
     */
	static protected $__instanceIntCountLimit = ConstInstance::PARAM_MULTITON_LIMIT;



	/**
	 * Default instance option.
     * @var string
     */
	static protected $__instanceStrOptionDefault = ConstInstance::OPTION_DEFAULT_LAST;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * @inheritdoc
     */
	public function __construct() 
	{
		// Set check allowed to create new instance
		static::instanceSetCheckNew();
		
		// Call parent constructor
		parent::__construct();
	}
	
	
	
	/**
	 * @inheritdoc
     */
	public function __clone()
    {
		// Set check allowed to create new instance
		static::instanceSetCheckNew();
		
		// Call parent clone
		parent::__clone();
    }
	
	
	
	
	
	// Methods check
	// ******************************************************************************
	
	/**
	 * Check allowed to create new instance.
	 * 
	 * @return boolean
     */
	public static function instanceCheckNew()
    {
		// Return result
		return (
            (static::instanceGetIntCountLimit() <= 0) ||
            (static::instanceGetIntCount() < static::instanceGetIntCountLimit())
        );
    }
	
	
	
	/**
	 * Set check allowed to create new instance.
	 *
	 * @throws InstanceNewForbiddenException
     */
	protected static function instanceSetCheckNew()
    {
		if(!static::instanceCheckNew())
		{
			throw new InstanceNewForbiddenException();
		}
    }
	

	
	
	
	// Methods getters
	// ******************************************************************************
	
	/**
	 * Get count limit of all instances.
	 * 
	 * @return integer
     */
	public static function instanceGetIntCountLimit()
    {
		// Return result
		return static::$__instanceIntCountLimit;
    }
	
	
	
	/**
	 * Get default option.
	 * 
	 * @return string
     */
	public static function instanceGetStrOptionDefault()
    {
		// Return result
		return static::$__instanceStrOptionDefault;
    }
	
	
	
	/**
	 * Get default instance.
	 * Get new instance, if impossible,
     * else get default instance (first or last instance, depending default option).
	 * 
	 * @return static
     */
	public static function instanceGetDefault()
    {
		// Init var
        $strOptionDefault = static::instanceGetStrOptionDefault();
        $result = (
            static::instanceCheckNew() ?
                // Get new instance
                forward_static_call_array(array(get_called_class(), 'instanceGetNew'), func_get_args()) :
                (
                    ($strOptionDefault == ConstInstance::OPTION_DEFAULT_FIRST) ?
                        // Get first instance
                        static::instanceGetFirst() :
                        // Get last instance
                        static::instanceGetLast()
                )
        );
		
		// Return result
		return $result;
    }
	
	
	
	
	
	// Methods setters
	// ******************************************************************************

	/**
	 * Set specified default option.
	 * 
     * @param string $strOptionDefault
	 * @throws OptionInvalidFormatException
     */
	public static function instanceSetStrOptionDefault($strOptionDefault)
    {
		// Set check format option
		OptionInvalidFormatException::setCheck($strOptionDefault);
		
		// Set index
		static::$__instanceStrOptionDefault = $strOptionDefault;
    }
	
	
	
}