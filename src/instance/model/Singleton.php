<?php
/**
 * Description :
 * This class allows to define singleton class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\instance\model;

use liberty_code\library\instance\exception\InstanceNewForbiddenException;



abstract class Singleton
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * Close access
     */
	protected function __construct() 
	{
		// Do nothing
	}
	
	
	
	/**
	 * Clone
	 * Abort process
     */
	final public function __clone()
    {
        throw new InstanceNewForbiddenException();
    }
	
	
	
	
	
	// Methods statics
	// ******************************************************************************
	
	/**
	 * Get single instance.
	 * 
	 * @return static
     */
	final static public function instanceGet()
    {
		// Init var: Declare this variable here because in property declaration, the extends doesn't work without a redeclaration of its.
		static $__objInstance;
		
		// Set instance if needs
		if(is_null($__objInstance))
		{
			$__objInstance = new static;  
		}
		
		// Return new instance
		return $__objInstance;
    }
	
	
	
}