<?php

namespace liberty_code\library\instance\test;

use liberty_code\library\instance\model\Instance;



class TestInstance extends Instance
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	/**
	 * Test
     * @var string
     */
	protected $strValue;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************

	public function __construct($strValue = '') 
	{
		parent::__construct();
		$this->setStrValue($strValue);
	}





    // Methods getters
    // ******************************************************************************

    public function getStrValue()
    {
        return $this->strValue;
    }





	// Methods setters
	// ******************************************************************************
	
	public function setStrValue($strValue)
	{
		$this->strValue = $strValue;
	}
	
	
	
} 