<?php

namespace liberty_code\library\instance\test;

use liberty_code\library\instance\library\ConstInstance;
use liberty_code\library\instance\test\TestMultiton;



class TestMultiton2 extends TestMultiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	/**
	 * Init instances table to dissociate this class from parent
     * @var int
     */
	static protected $__instanceIntCountLimit = 2;



	/**
	 * Init option default instance to dissociate this class from parent
     * @var string
     */
	static protected $__instanceStrOptionDefault = ConstInstance::OPTION_DEFAULT_LAST;
	
	
	
	/**
	 * Test
     * @var string
     */
	protected $strValue2;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************

	public function __construct($strValue = '', $strValue2 = '') 
	{
		parent::__construct();
		$this->setStrValue($strValue);
		$this->setStrValue2($strValue2);
	}





    // Methods getters
    // ******************************************************************************

    public function getStrValue2()
    {
        return $this->strValue2;
    }





	// Methods setters
	// ******************************************************************************
	
	public function setStrValue2($strValue2)
	{
		$this->strValue2 = $strValue2;
	}
	
	
	
} 