<?php

namespace liberty_code\library\instance\test;

use liberty_code\library\instance\library\ConstInstance;
use liberty_code\library\instance\model\Multiton;



class TestMultiton extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	/**
	 * Init instances table to dissociate this class from parent
     * @var int
     */
	static protected $__instanceIntCountLimit = 3;



	/**
	 * Init option default instance table to dissociate this class from parent
     * @var string
     */
	static protected $__instanceStrOptionDefault = ConstInstance::OPTION_DEFAULT_LAST;
	
	
	
	/**
	 * Test
     * @var string
     */
	protected $strValue;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************

	public function __construct($strValue = '') 
	{
		parent::__construct();
		$this->setStrValue($strValue);
	}





    // Methods getters
    // ******************************************************************************

    public function getStrValue()
    {
        return $this->strValue;
    }



	
	
	// Methods setters
	// ******************************************************************************
	
	public function setStrValue($strValue)
	{
		$this->strValue = $strValue;
	}
	
	
	
} 