<?php

namespace liberty_code\library\instance\test;

use liberty_code\library\instance\model\Singleton;



class TestSingleton2 extends Singleton
{
	/**
	 * Test
     * @var string
     */
	protected $strValue2;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************

	public function __construct()
	{
		$this->setStrValue('singleton - inst 1');
	}





    // Methods getters
    // ******************************************************************************

    public function getStrValue()
    {
        return $this->strValue;
    }




	
	// Methods setters
	// ******************************************************************************
	
	public function setStrValue($strValue)
	{
		$this->strValue = $strValue;
	}
	
	
	
} 