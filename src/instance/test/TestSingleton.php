<?php

namespace liberty_code\library\instance\test;

use liberty_code\library\instance\library\ConstInstance;
use liberty_code\library\instance\model\Multiton;



class TestSingleton extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Extend this protperties to set singleton.
	 * Call TestSingleton::instanceGetDefault(...constructor paramters) the first time to call with constructor paramters
	 * Call TestSingleton::instanceGetDefault() constructor parameters not necessary after
     */
	 
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	/**
	 * Init instances limitation to dissociate this class from parent
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;



	/**
	 * Init option default instance table to dissociate this class from parent
     * @var string
     */
	static protected $__instanceStrOptionDefault = ConstInstance::OPTION_DEFAULT_LAST;
	
	
	
	/**
	 * Test
     * @var string
     */
	protected $strValue;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************

	public function __construct($strValue) 
	{
		parent::__construct();
		$this->setStrValue($strValue);
	}





    // Methods getters
    // ******************************************************************************

    public function getStrValue()
    {
        return $this->strValue;
    }



    // Get singleton with constructor parameters
    public static function getObjSingleton()
    {
        $objTestSingleton = null;
        if(TestSingleton::instanceCheckNew())
        {
            $objTestSingleton = TestSingleton::instanceGetDefault('singleton - inst 1');
        }
        else
        {
            $objTestSingleton = TestSingleton::instanceGetDefault();
        }

        return $objTestSingleton;
    }





	// Methods setters
	// ******************************************************************************
	
	public function setStrValue($strValue)
	{
		$this->strValue = $strValue;
	}
	
	
	
} 