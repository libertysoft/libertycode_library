<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
include($strRootAppPath . '/src/instance/test/TestInstance.php');
include($strRootAppPath . '/src/instance/test/TestInstance2.php');
include($strRootAppPath . '/src/instance/test/TestMultiton.php');
include($strRootAppPath . '/src/instance/test/TestMultiton2.php');
include($strRootAppPath . '/src/instance/test/TestSingleton.php');
include($strRootAppPath . '/src/instance/test/TestSingleton2.php');

// Use
use liberty_code\library\instance\model\Instance;
use liberty_code\library\instance\test\TestInstance;
use liberty_code\library\instance\test\TestInstance2;
use liberty_code\library\instance\test\TestMultiton;
use liberty_code\library\instance\test\TestMultiton2;
use liberty_code\library\instance\test\TestSingleton;
use liberty_code\library\instance\test\TestSingleton2;



// Init var test instance
$objTestInstance1_1 = new TestInstance('cls 1 - inst 1');
echo('Create: '.TestInstance::instanceGetLast()->getStrValue().' - ');
echo('First: '.TestInstance::instanceGetFirst()->getStrValue().' - ');
echo('Count Instance: |'.Instance::instanceGetIntCount().'| - ');
echo('Count TestInstance: |'.TestInstance::instanceGetIntCount().'|');
echo('<br />');

$objTestInstance1_2 = new TestInstance('cls 1 - inst 2');
echo('Create: '.TestInstance::instanceGetLast()->getStrValue().' - ');
echo('First: '.TestInstance::instanceGetFirst()->getStrValue().' - ');
echo('Count Instance: |'.Instance::instanceGetIntCount().'| - ');
echo('Count TestInstance: |'.TestInstance::instanceGetIntCount().'|');
echo('<br />');

$objTestInstance1_3 = new TestInstance('cls 1 - inst 3');
echo('Create: '.TestInstance::instanceGetLast()->getStrValue().' - ');
echo('First: '.TestInstance::instanceGetFirst()->getStrValue().' - ');
echo('Count Instance: |'.Instance::instanceGetIntCount().'| - ');
echo('Count TestInstance: |'.TestInstance::instanceGetIntCount().'|');
echo('<br />');

echo('<br /><br /><br />');



// Init var test instance 2
$objTestInstance2_1 = new TestInstance2('cls 2', 'inst 1');
echo('Create: '.TestInstance2::instanceGetLast()->getStrValue().'/'.TestInstance2::instanceGetLast()->getStrValue2().' - ');
echo('First: '.TestInstance2::instanceGetFirst()->getStrValue().'/'.TestInstance2::instanceGetFirst()->getStrValue2().' - ');
echo('Count Instance: |'.Instance::instanceGetIntCount().'| - ');
echo('Count TestInstance: |'.TestInstance::instanceGetIntCount().'| - ');
echo('Count TestInstance2: |'.TestInstance2::instanceGetIntCount().'|');
echo('<br />');

$objTestInstance2_2 = new TestInstance2('cls 2', 'inst 2');
echo('Create: '.TestInstance2::instanceGetLast()->getStrValue().'/'.TestInstance2::instanceGetLast()->getStrValue2().' - ');
echo('First: '.TestInstance2::instanceGetFirst()->getStrValue().'/'.TestInstance2::instanceGetFirst()->getStrValue2().' - ');
echo('Count Instance: |'.Instance::instanceGetIntCount().'| - ');
echo('Count TestInstance: |'.TestInstance::instanceGetIntCount().'| - ');
echo('Count TestInstance2: |'.TestInstance2::instanceGetIntCount().'|');
echo('<br />');

echo('<br /><br /><br />');



// Test creation instance dynamically
$objTestInstance2_3 = TestInstance2::instanceGetNew('cls 2', 'inst 3');
echo('Create: '.$objTestInstance2_3->getStrValue().'/'.TestInstance2::instanceGetLast()->getStrValue2().' - ');
echo('First: '.$objTestInstance2_3::instanceGetFirst()->getStrValue().'/'.TestInstance2::instanceGetFirst()->getStrValue2().' - ');
echo('Count Instance: |'.Instance::instanceGetIntCount().'| - ');
echo('Count TestInstance: |'.TestInstance::instanceGetIntCount().'| - ');
echo('Count TestInstance2: |'.TestInstance2::instanceGetIntCount().'|');
echo('<br />');

echo('<br /><br /><br />');



// Get instances
//echo('<pre>');var_dump(TestInstance2::instanceGet(1));echo('</pre>');
//echo('<pre>');var_dump(TestInstance::instanceGet(1));echo('</pre>');
//echo('<pre>');var_dump(Instance::instanceGet(1));echo('</pre>');

//echo('<br /><br /><br />');



// Test creation instance multiton
$objTestMultiton1_1 = TestMultiton::instanceGetNew('cls 1 - inst 1');
echo('Create multiton: '.TestMultiton::instanceGetLast()->getStrValue().' - ');
echo('First: '.TestMultiton::instanceGetFirst()->getStrValue().' - ');
echo('Count Instance: |'.Instance::instanceGetIntCount().'| - ');
echo('Count TestMultiton: |'.TestMultiton::instanceGetIntCount().'|');
echo('<br />');

$objTestMultiton1_2 = new TestMultiton('cls 1 - inst 2');
echo('Create multiton: '.TestMultiton::instanceGetLast()->getStrValue().' - ');
echo('First: '.TestMultiton::instanceGetFirst()->getStrValue().' - ');
echo('Count Instance: |'.Instance::instanceGetIntCount().'| - ');
echo('Count TestMultiton: |'.TestMultiton::instanceGetIntCount().'|');
echo('<br />');

$objTestMultiton1_3 = TestMultiton::instanceGetNew('cls 1 - inst 3');
echo('Create multiton: '.TestMultiton::instanceGetLast()->getStrValue().' - ');
echo('First: '.TestMultiton::instanceGetFirst()->getStrValue().' - ');
echo('Count Instance: |'.Instance::instanceGetIntCount().'| - ');
echo('Count TestMultiton: |'.TestMultiton::instanceGetIntCount().'|');
echo('<br />');

try
{
	$objTestMultiton1_4 = TestMultiton::instanceGetNew('cls 1 - inst 4');
}
catch(Exception $e)
{
	echo('Creation impossible TestMultiton: cls 1 - inst 4');
	echo('<br />');
}

try
{
	$objTestMultiton1_5 = new TestMultiton('cls 1 - inst 5');
	echo('Create multiton: '.TestMultiton::instanceGetLast()->getStrValue().' - ');
	echo('First: '.TestMultiton::instanceGetFirst()->getStrValue().' - ');
	echo('Count Instance: |'.Instance::instanceGetIntCount().'| - ');
	echo('Count TestMultiton: |'.TestMultiton::instanceGetIntCount().'|');
	echo('<br />');
}
catch(Exception $e)
{
	echo('Creation impossible TestMultiton: cls 1 - inst 5 '.$e->getMessage());
	echo('<br />');
}

$objTestMultiton1_6 = TestMultiton::instanceGetDefault('cls 1 - inst 6');
echo('See multiton: '.$objTestMultiton1_6->getStrValue().' - ');
echo('First: '.TestMultiton::instanceGetFirst()->getStrValue().' - ');
echo('Count Instance: |'.Instance::instanceGetIntCount().'| - ');
echo('Count TestMultiton: |'.TestMultiton::instanceGetIntCount().'|');

echo('<br /><br /><br />');



// Test creation instance multiton 2
$objTestMultiton2_1 = TestMultiton2::instanceGetDefault('cls 2 - inst 1');
echo('Create multiton2: '.TestMultiton2::instanceGetLast()->getStrValue().' - ');
echo('First: '.TestMultiton2::instanceGetFirst()->getStrValue().' - ');
echo('Count Instance: |'.Instance::instanceGetIntCount().'| - ');
echo('Count TestMultiton: |'.TestMultiton::instanceGetIntCount().'| - ');
echo('Count TestMultiton2: |'.TestMultiton2::instanceGetIntCount().'|');
echo('<br />');

$objTestMultiton2_2 = new TestMultiton2('cls 2 - inst 2');
echo('Create multiton2: '.TestMultiton2::instanceGetLast()->getStrValue().' - ');
echo('First: '.TestMultiton2::instanceGetFirst()->getStrValue().' - ');
echo('Count Instance: |'.Instance::instanceGetIntCount().'| - ');
echo('Count TestMultiton: |'.TestMultiton::instanceGetIntCount().'| - ');
echo('Count TestMultiton2: |'.TestMultiton2::instanceGetIntCount().'|');
echo('<br />');

try
{
	$objTestMultiton2_3 = new TestMultiton2('cls 2 - inst 3');
	echo('Create multiton2: '.TestMultiton2::instanceGetLast()->getStrValue().' - ');
	echo('First: '.TestMultiton2::instanceGetFirst()->getStrValue().' - ');
	echo('Count Instance: |'.Instance::instanceGetIntCount().'| - ');
	echo('Count TestMultiton: |'.TestMultiton::instanceGetIntCount().'| - ');
	echo('Count TestMultiton2: |'.TestMultiton2::instanceGetIntCount().'|');
	echo('<br />');
}
catch(Exception $e)
{
	echo('Creation impossible TestMultiton2: cls 2 - inst 3 '.$e->getMessage());
	echo('<br />');
}

echo('<br /><br /><br />');



// Test singleton
echo('See singleton 1 time: '.TestSingleton::getObjSingleton()->getStrValue().' - ');
echo('Count Instance: |'.TestSingleton::instanceGetIntCount().'|');
echo('<br />');

echo('See singleton 2 time: '.TestSingleton::getObjSingleton()->getStrValue().' - ');
echo('Count Instance: |'.TestSingleton::instanceGetIntCount().'|');
echo('<br />');

echo('See singleton 3 time: '.TestSingleton::instanceGetDefault('singleton - inst 1')->getStrValue().' - ');
echo('Count Instance: |'.TestSingleton::instanceGetIntCount().'|');
echo('<br />');

echo('See singleton 4 time: '.TestSingleton::instanceGetDefault()->getStrValue().' - ');
echo('Count Instance: |'.TestSingleton::instanceGetIntCount().'|');
echo('<br />');

echo('<br /><br /><br />');



// Test singleton 2
echo('See singleton2 1 time: '.TestSingleton2::instanceGet()->getStrValue().'');
echo('<br />');

echo('See singleton2 2 time: '.TestSingleton2::instanceGet()->getStrValue().'');
echo('<br />');

echo('See singleton2 3 time: '.TestSingleton2::instanceGet()->getStrValue().'');
echo('<br />');

echo('<br /><br /><br />');


