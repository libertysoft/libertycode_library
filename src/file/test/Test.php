<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\library\file\library\ToolBoxFile;



// Test file path get
$tabPath = array(
    ['test', false], // Ko: file not exists
    ['test/test_2', true], // Ko: folder not exists
    [$strRootAppPath, true], // Ok
    [$strRootAppPath, false], // Ko: exists but is folder
    [$strRootAppPath . '/composer.json', false], // Ok
    [$strRootAppPath . '/composer.json', true] // Ko: exists but is file
);

foreach($tabPath as $tabPathInfo)
{
    // Get info
    $strPath = $tabPathInfo[0];
    $boolIsFolder = $tabPathInfo[1];

    echo('Test ' . ($boolIsFolder ? 'folder' : 'file') . ' path get: "' . $strPath . '": <br />');
    echo('Check: <pre>');var_dump(ToolBoxFile::checkFilePathValid($strPath, $boolIsFolder));echo('</pre>');
    echo('Get: <pre>');var_dump(ToolBoxFile::getStrFilePath($strPath, $boolIsFolder));echo('</pre>');

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


