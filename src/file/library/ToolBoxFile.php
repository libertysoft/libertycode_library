<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\file\library;

use liberty_code\library\instance\model\Multiton;



class ToolBoxFile extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified file path is valid.
     *
     * @param string $strFilePath
     * @param boolean $boolIsFolder = false
     * @return boolean
     */
    public static function checkFilePathValid($strFilePath, $boolIsFolder = false)
    {
        // Return result
        return (!is_null(static::getStrFilePath($strFilePath, $boolIsFolder)));
    }





	// Methods getters
	// ******************************************************************************

    /**
     * Get specified file path, if valid.
     *
     * @param string $strFilePath
     * @param boolean $boolIsFolder = false
     * @return null|string
     */
    public static function getStrFilePath($strFilePath, $boolIsFolder = false)
    {
        // Init var
        $boolIsFolder = (is_bool($boolIsFolder) ? $boolIsFolder : false);
        $result = (
            (
                is_string($strFilePath) &&
                (
                    ((!$boolIsFolder) && is_file($strFilePath)) || // File case
                    ($boolIsFolder && is_dir($strFilePath)) // Folder case
                )
            ) ?
            $strFilePath :
            null
        );

        // Return result
        return $result;
    }



}