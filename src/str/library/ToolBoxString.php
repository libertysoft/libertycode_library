<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\str\library;

use Exception;
use liberty_code\library\instance\model\Multiton;



class ToolBoxString extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified encoding name exists.
     *
     * @param string $strName
     * @return boolean
     */
    public static function checkEncodingNameExists($strName)
    {
        // Return result
        return (
            is_string($strName) &&
            in_array($strName, mb_list_encodings())
        );
    }



    /**
     * Check if specified value can be converted to string.
     *
     * @param mixed $value
     * @return boolean
     */
    public static function checkConvertString($value)
    {
        // Init var
        $result = (
            // Check not array
            (!is_array($value)) &&

            // Check valid type
            (
                // Check valid simple type
                (
                    (!is_object($value)) &&
                    (@settype($value, 'string') !== false)
                ) ||
                // Check valid object
                (
                    is_object($value) &&
                    method_exists($value, '__toString')
                )
            )
        );

        // Case object: check no exception found
        if($result && is_object($value))
        {
            try
            {strval($value->__toString());}
            catch(Exception $e)
            {$result = false;}
        }

        // Return result
        return $result;
    }



    /**
     * Check if specified value starts with specified start value.
     *
     * @param string $strValue
     * @param string $strStartValue
     * @param string $strEncodingName = null
     * @return boolean
     */
    public static function checkStartsWith($strValue, $strStartValue, $strEncodingName = null)
    {
        // Init var
        $strEncodingName = static::getStrEncodingName($strEncodingName);
        $result = (
            is_string($strValue) &&
            is_string($strStartValue) &&
            (mb_substr($strValue, 0, mb_strlen($strStartValue, $strEncodingName), $strEncodingName) === $strStartValue)
        );

        // Return result
        return $result;
    }



    /**
     * Check if specified value ends with specified end value.
     *
     * @param string $strValue
     * @param string $strEndValue
     * @param string $strEncodingName = null
     * @return boolean
     */
    public static function checkEndsWith($strValue, $strEndValue, $strEncodingName = null)
    {
        // Init var
        $strEncodingName = static::getStrEncodingName($strEncodingName);
        $result = (
            is_string($strValue) &&
            is_string($strEndValue) &&
            (mb_substr($strValue, (-1 * mb_strlen($strEndValue, $strEncodingName)), null, $strEncodingName) === $strEndValue)
        );

        // Return result
        return $result;
    }



    /**
     * Check if specified value contains specified contain value.
     *
     * @param string $strValue
     * @param string $strContainValue
     * @param string $strEncodingName = null
     * @return boolean
     */
    public static function checkContains($strValue, $strContainValue, $strEncodingName = null)
    {
        // Init var
        $strEncodingName = static::getStrEncodingName($strEncodingName);
        $result = (
            is_string($strValue) &&
            is_string($strContainValue) &&
            (mb_strpos($strValue, $strContainValue, 0, $strEncodingName) !== false)
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get encoding name.
     * Return specified encoding name if exists,
     * default encoding name else.
     *
     * @param string $strName
     * @return string
     */
    public static function getStrEncodingName($strName = null)
    {
        // Return result
        return (
            static::checkEncodingNameExists($strName) ?
                $strName :
                mb_internal_encoding()
        );
    }



}