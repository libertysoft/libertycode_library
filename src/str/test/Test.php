<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\library\str\library\ToolBoxString;



// Test string compare
$tabInfo = array(
	[
		1,
		'test 1'
	],

	[
		'Value test 1',
		'test 1'
	],
	[
		'Value test 1',
		1
	],
    [
        'test 1 Value',
        'test 1'
    ],
    [
        'test 1 Value',
        '1'
    ],
    [
        array('key_1' => 'Value 1'),
        '1'
    ],
    [
        new DateTime(),
        '1'
    ]
);

foreach($tabInfo as $info)
{
    echo('Test string compare: <br />');
    echo('Info: <pre>');var_dump($info);echo('</pre>');

	// Get info
	$value = $info[0];
    $compareValue = $info[1];

    $boolConvertString = ToolBoxString::checkConvertString($value);
    $boolStartWith = ToolBoxString::checkStartsWith($value, $compareValue, 'ISO-8859-1');
    $boolEndWith = ToolBoxString::checkEndsWith($value, $compareValue, 'UTF-8');
    $boolContain = ToolBoxString::checkContains($value, $compareValue, 'test');

    echo('Encoding name: <pre>');var_dump(ToolBoxString::getStrEncodingName());echo('</pre>');
    echo('Check convert to string: <pre>');var_dump($boolConvertString);echo('</pre>');
    echo('Check starts with: <pre>');var_dump($boolStartWith);echo('</pre>');
    echo('Check ends with: <pre>');var_dump($boolEndWith);echo('</pre>');
    echo('Check contains: <pre>');var_dump($boolContain);echo('</pre>');

	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


