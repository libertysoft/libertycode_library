<?php

namespace liberty_code\library\reflection\test1;

interface InterfaceTest1
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     * @param $intArg1
     * @param $strArg2
     * @param $boolArg3
     * @param $tabArg4
     */
    public function __construct($intArg1, $strArg2, $boolArg3, array $tabArg4 = array());



    // Methods getters
    // ******************************************************************************

    public function getTabArg();
}



class ClassTest1 implements InterfaceTest1
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var array */
    protected $tabArg;



    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct($intArg1, $strArg2, $boolArg3, array $tabArg4 = array())
    {
        // Init var
        $this->tabArg = array(
            'arg_1' => $intArg1,
            'arg_2' => $strArg2,
            'arg_3' => $boolArg3,
            'arg_4' => $tabArg4
        );
    }



    // Methods getters
    // ******************************************************************************

    public function getTabArg()
    {
        // Return result
        return $this->tabArg;
    }



    public function getStrHash()
    {
        // Return result
        return spl_object_hash($this);
    }
}



class   ClassTest1_Bis{}





namespace liberty_code\library\reflection\test2;

use liberty_code\library\reflection\test1\InterfaceTest1;
use liberty_code\library\reflection\test1\ClassTest1;

class ClassTest2 extends ClassTest1 implements InterfaceTest1
{
}





namespace liberty_code\library\reflection\test3;use liberty_code\library\reflection\test2\ClassTest2;abstract class Class_Test3 extends ClassTest2{}