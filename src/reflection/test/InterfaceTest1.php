<?php

namespace liberty_code\library\reflection\test;



interface InterfaceTest1
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************
	
	// Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     */
	public function __construct($intArg1, $strArg2, $boolArg3, array $tabArg4 = array());
	
	
	
	
	
    // Methods getters
    // ******************************************************************************

    public function getTabArg();
}