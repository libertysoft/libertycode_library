<?php

namespace liberty_code\library\reflection\test;

use liberty_code\library\reflection\test\InterfaceTest1;



class ClassTest1 implements InterfaceTest1
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var array */
    protected $tabArg;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     */
    public function __construct($intArg1, $strArg2, $boolArg3, array $tabArg4 = array())
    {
        // Init var
        $this->tabArg = array(
            'arg_1' => $intArg1,
            'arg_2' => $strArg2,
            'arg_3' => $boolArg3,
            'arg_4' => $tabArg4
        );
    }





    // Methods getters
    // ******************************************************************************

    public function getTabArg()
    {
        // Return result
        return $this->tabArg;
    }



    public function getStrHash()
    {
        // Return result
        return spl_object_hash($this);
    }





    // Test reflection function
    // ******************************************************************************

    private function testMethod($arg1, $arg2)
    {
        echo sprintf('m[%1$s:%2$s]', strval($arg1), strval($arg2));
    }



}