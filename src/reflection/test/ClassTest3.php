<?php

namespace liberty_code\library\reflection\test;



class ClassTest3
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var array */
    protected $tabArg;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     */
    protected function __construct($intArg1, $strArg2, $boolArg3, array $tabArg4 = array())
    {
        // Init var
        $this->tabArg = array(
            'arg_1' => $intArg1,
            'arg_2' => $strArg2,
            'arg_3' => $boolArg3,
            'arg_4' => $tabArg4
        );
    }





    // Methods getters
    // ******************************************************************************

    public function getTabArg()
    {
        // Return result
        return $this->tabArg;
    }



}