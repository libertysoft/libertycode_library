<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
include(dirname(__FILE__) . '/InterfaceTest1.php');
include(dirname(__FILE__) . '/ClassTest1.php');
include(dirname(__FILE__) . '/ClassTest2.php');

// Use
use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\library\reflection\library\ToolBoxClassReflection;
use liberty_code\library\reflection\library\ToolBoxFileReflection;
use liberty_code\library\reflection\test\ClassTest1;
use liberty_code\library\reflection\test\ClassTest2;



// Test reflection
$tabClass = array(
	// Ko: Instantiation impossible (constructor impossible to get, on previous PHP versions)
	[
		'liberty_code\library\reflection\test\InterfaceTest1',
		[7, 'test 1', true]
	],
	
	// Ok
	[
		'liberty_code\library\reflection\test\ClassTest1',
		[7, 'test 1', true]
	],
	
	// Ko: Instantiation invalid
	[
		'liberty_code\library\reflection\test\ClassTest1',
		[7, 'test 1', true, 'test']
	],
	
	// Ko: Instantiation invalid
	[
		'liberty_code\library\reflection\test\ClassTest1',
		[7, 'test 1', true, array(), 'test']
	],
	
	// Ok
	[
		'liberty_code\library\reflection\test\ClassTest2',
		[new ClassTest1(9, 'test 1_1', false), 'test 2', true]
	],
	
	// Ko: Class not found
	[
		'liberty_code\library\reflection\test\ClassTest22',
		[new ClassTest1(9, 'test 1_1', false), 'test 2', true]
	],

    // Ok
    [
        'liberty_code\library\reflection\test\ClassTest3',
        [3, 'test 3', false]
    ]
);

foreach($tabClass as $tabClassInfo)
{
	// Get info
	$strClassPath = $tabClassInfo[0];
	$tabArg = $tabClassInfo[1];
	
	echo('Test reflection "'.$strClassPath.'": <br />');
	
	// Get instance
	$objInstance = ToolBoxReflection::getObjInstance($strClassPath, $tabArg, true);
	if(!is_null($objInstance))
	{
		echo('Get: instance: <pre>');print_r($objInstance);echo('</pre>');
	}
	else
	{
		echo('Get: instance (null): <pre>');var_dump($objInstance);echo('</pre>');
	}
	
	// Check constructor
	try{
		$objClass = new \ReflectionClass($strClassPath);
		$objConstruct = $objClass->getConstructor();
		
		if(!is_null($objConstruct))
		{
			$boolCheck = ToolBoxReflection::checkFunctionArgValid($objClass->getConstructor(), $tabArg);
			
			echo('Check: <pre>');var_dump($boolCheck);echo('</pre>');
		}
		else
		{
			echo('Constructor unfound!<br />');
		}
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test reflection function
function testFunction($arg1, $arg2)
{
    return sprintf('f{%1$s:%2$s}', strval($arg1), strval($arg2));
}

$tabConfig = array(
    // Ko: not static
    [
        ['liberty_code\library\reflection\test\ClassTest1', 'testMethod'],
        [1, 'test 1']
    ],

    // Ok
    [
        [new ClassTest1(9, 'test 1_1', false), 'testMethod'],
        [1, 'test 1']
    ],

    // Ko: function not found
    [
        '_testFunction',
        [2, 'test 2']
    ],

    // Ok
    [
        'testFunction',
        [2, 'test 2']
    ],

    // Ok
    [
        ['liberty_code\library\reflection\test\ClassTest2', 'testStaticMethod'],
        [3, 'test 3']
    ],

    // Ok
    [
        [
            new ClassTest2(
                new ClassTest1(9, 'test 1_1', false),
                'test 2',
                true
            ),
            'testStaticMethod'
        ],
        [3, 'test 3 bis']
    ]
);

echo('Test reflection function: <br />');
foreach($tabConfig as $config)
{
    // Get info
    $configFunction = $config[0];
    $tabArg = $config[1];

    echo('Function config: <pre>');var_dump($configFunction);echo('</pre>');
    echo('Arguments: <pre>');var_dump($tabArg);echo('</pre>');

    // Get reflection function object
    $objFunction = ToolBoxReflection::getObjFunction($configFunction);
    if(!is_null($objFunction))
    {
        echo('Reflection object: <pre>');var_dump(get_class($objFunction));echo('</pre>');
    }
    else
    {
        echo('Reflection object!');
    }

    // Get callable
    $callFunction = ToolBoxReflection::getCallFunction($configFunction, $tabArg, true);
    if(!is_null($callFunction))
    {
        echo('Callable OK:');
        $callFunctionReturn = $callFunction();
        echo('<pre>');var_dump($callFunctionReturn);echo('</pre>');
    }
    else
    {
        echo('Callable KO!');
    }

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test class reflection
echo('Test class reflection: <br />');

$tabClassPath = array(
    ToolBoxReflection::class,
    ToolBoxClassReflection::class,
    ToolBoxFileReflection::class,
    ClassTest1::class,
    ClassTest2::class
);

foreach($tabClassPath as $strClassPath)
{
    // Get info
    $strNamespace = ToolBoxClassReflection::getStrNamespace($strClassPath);
    $strClassName = ToolBoxClassReflection::getStrClassName($strClassPath);

    echo('Class path: <pre>');echo($strClassPath);echo('</pre>');
    echo('Class namespace: <pre>');echo($strNamespace);echo('</pre>');
    echo('Class name: <pre>');echo($strClassName);echo('</pre>');

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test file reflection
echo('Test file reflection: <br />');

$tabFilePath = array(
    $strRootAppPath . '/src/reflection/test/InterfaceTest1.php',
    $strRootAppPath . '/src/reflection/test/ClassTest1.php',
    $strRootAppPath . '/src/reflection/test/ClassTest2.php',
    $strRootAppPath . '/src/reflection/test/ClassTest3.php',
    $strRootAppPath . '/src/reflection/test/TestClasses1.php',
    $strRootAppPath . '/src/reflection/test/TestClasses2.php'
);

foreach($tabFilePath as $strFilePath)
{
    // Get info
    $tabNamespace = ToolBoxFileReflection::getTabNamespace($strFilePath);
    $tabClassPath = ToolBoxFileReflection::getTabClassPath($strFilePath);

    echo('File path: <pre>');echo($strFilePath);echo('</pre>');

    // Get info
    $tabNamespace = ToolBoxFileReflection::getTabNamespace($strFilePath);
    $tabClassPath = ToolBoxFileReflection::getTabClassPath($strFilePath);
    $tabInterfacePath = ToolBoxFileReflection::getTabInterfacePath($strFilePath);

    echo('Get array of namespaces: <pre>');print_r($tabNamespace);echo('</pre>');
    echo('Get array of class paths: <pre>');print_r($tabClassPath);echo('</pre>');
    echo('Get array of interface paths: <pre>');print_r($tabInterfacePath);echo('</pre>');

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


