<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\reflection\library;

use liberty_code\library\instance\model\Multiton;



class ToolBoxClassReflection extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get class namespace,
     * from specified class path.
     *
     * @param string $strClassPath
     * @return string
     */
	public static function getStrNamespace($strClassPath)
	{
        // Return result
        return (
            is_string($strClassPath) ?
                str_replace(
                    '/',
                    '\\',
                    pathinfo(
                        str_replace('\\', '/', $strClassPath),
                        PATHINFO_DIRNAME
                    )
                ) :
                null
        );
	}



    /**
     * Get class name,
     * from specified class path.
     *
     * @param string $strClassPath
     * @return null|string
     */
    public static function getStrClassName($strClassPath)
    {
        // Return result
        return (
            is_string($strClassPath) ?
                basename(str_replace('\\', '/', $strClassPath)) :
                null
        );
    }



}