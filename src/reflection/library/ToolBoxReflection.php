<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\reflection\library;

use liberty_code\library\instance\model\Multiton;

use ReflectionClass;
use ReflectionFunctionAbstract;
use ReflectionFunction;
use ReflectionMethod;
use ReflectionException;



class ToolBoxReflection extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods check
	// ******************************************************************************

    /**
     * Check if specified arguments are valid,
     * for specified function.
     *
     * @param ReflectionFunctionAbstract $objFunction
     * @param array $tabArg = array()
     * @return boolean
     */
    public static function checkFunctionArgValid(ReflectionFunctionAbstract $objFunction, array $tabArg = array())
    {
        // Init var
        $result = false;
        $tabParam = $objFunction->getParameters();
        $tabArg = array_values($tabArg);

        // Check size valid
        if(count($tabArg) <= ($intParamCount = count($tabParam)))
        {
            // Init var
            $result = true;

            // Run all parameters
            for($intCpt = 0; ($intCpt < $intParamCount) && $result; $intCpt++) {
                // Get info
                $objParam = $tabParam[$intCpt];
                $objParamType = $objParam->getType();
                $strParamType = (
                    (!is_null($objParamType)) ?
                        (
                            method_exists($objParamType, 'getName') ?
                                $objParamType->getName() :
                                strval($objParamType)
                        ) :
                        null
                );
                $boolArgExists = (
                    isset($tabArg[$intCpt]) ||  // Check via isset first (more fast)
                    array_key_exists($intCpt, $tabArg)
                );
                $arg = $boolArgExists ? $tabArg[$intCpt] : null;

                // Set validation
                $result =
                    // Case argument not found
                    (
                        (!$boolArgExists) &&

                        // Default value found
                        $objParam->isDefaultValueAvailable()
                    ) ||

                    // Case argument found
                    (
                        $boolArgExists &&
                        (
                            // No type specified
                            is_null($objParamType) ||

                            // Type specified
                            (
                                is_object($arg) ?
                                    // Check argument class, if required
                                    (
                                        (get_class($arg) === $strParamType) ||
                                        is_subclass_of($arg, $strParamType)
                                    ) :
                                    // Check argument type, if required
                                    (gettype($arg) === $strParamType)
                            ) ||

                            // Default value specified: check default argument type
                            (
                                $objParam->isDefaultValueAvailable() &&
                                (!is_object($paramDefaultValue = $objParam->getDefaultValue())) &&
                                (!is_object($arg)) &&
                                (gettype($paramDefaultValue) === gettype($arg))
                            )
                        )
                    );
            }
        }

        // Return result
        return $result;
    }



	
	
	// Methods getters
	// ******************************************************************************
	
	/**
	 * Get object instance,
     * from specified class and arguments.
	 * 
	 * @param string $strClassPath
	 * @param array $tabArg = array()
     * @param boolean $boolForceAccess = false : Allows to force access in case of private or protected constructor.
	 * @return null|mixed
     * @throws ReflectionException
	 */
	public static function getObjInstance($strClassPath, array $tabArg = array(), $boolForceAccess = false)
	{
		// Init var
		$result = null;
        $boolForceAccess = (is_bool($boolForceAccess) ? $boolForceAccess : false);
		
		// Check arguments valid
		if(is_string($strClassPath) && class_exists($strClassPath))
		{
			// Get info
            $objClass = new ReflectionClass($strClassPath);
            $objConstruct = $objClass->getConstructor();
            
			// Check constructor found and arguments valid
			if((!is_null($objConstruct)) && static::checkFunctionArgValid($objConstruct, $tabArg))
			{
				// Create instance
                $result = $objClass->newInstanceWithoutConstructor();
                $objConstruct->setAccessible($boolForceAccess);
                $objConstruct->invokeArgs($result, $tabArg);
			}
			// Check no constructor found and no argument provided
			else if(is_null($objConstruct) && (count($tabArg) === 0))
			{
				// Create instance
				$result = $objClass->newInstance();
			}
		}
		
		// Return result
		return $result;
	}



    /**
     * Get reflection function object,
     * from specified configuration.
     * Instance can be returned, from configuration.
     *
     * Configuration format:
     * 'string function name'
     * OR
     * [
     *     object,
     *     'string method name'
     * ]
     * OR
     * [
     *     object OR 'string class path',
     *     'string static method name'
     * ]
     *
     * @param string|array $config
     * @param null|object &$objInstance = null
     * @return null|ReflectionFunction|ReflectionMethod
     */
    public static function getObjFunction($config, &$objInstance = null)
    {
        // Init var
        $result = null;
        $objInstance = null;

        // Get function reflection object, if required
        if(
            // Case function
            is_string($config) &&
            function_exists($config)
        )
        {
            $result = new ReflectionFunction($config);
        }
        // Get method reflection object, if required
        else if(
            isset($config[0]) &&
            isset($config[1]) &&
            is_string($config[1])
        )
        {
            $objInstance = (is_object($config[0]) ? $config[0] : null);
            $objClass = (
                (!is_null($objInstance)) ?
                    new ReflectionClass($objInstance) :
                    (
                        (is_string($config[0]) && class_exists($config[0])) ?
                            new ReflectionClass($config[0]) :
                            null
                    )
            );
            $objMethod = (
                (
                    (!is_null($objClass)) &&
                    ($objClass->hasMethod($config[1]))
                ) ?
                    $objClass->getMethod($config[1]) :
                    null
            );
            $result = (
                (
                    (!is_null($objMethod)) &&
                    ((!is_null($objInstance)) || $objMethod->isStatic())
                ) ?
                    $objMethod :
                    null
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get callback function,
     * to execute specified function configuration,
     * with specified index array of arguments.
     *
     * Function configuration format:
     * @see getObjFunction() configuration format.
     *
     * Callable return format:
     * mixed function(): if able to define it,
     * null else.
     *
     * @param string|array $functionConfig
     * @param array $tabArg = array()
     * @param boolean $boolForceAccess = false : Allows to force access in case of private or protected method.
     * @return null|callable
     */
    public static function getCallFunction($functionConfig, array $tabArg = array(), $boolForceAccess = false)
    {
        // Init var
        $boolForceAccess = (is_bool($boolForceAccess) ? $boolForceAccess : false);
        $objInstance = null;
        $objFunction = static::getObjFunction($functionConfig, $objInstance);
        $result = (
            (
                (!is_null($objFunction)) &&
                static::checkFunctionArgValid($objFunction, $tabArg)
            ) ?
                // Set callback function, if required
                function () use ($objFunction, $tabArg, $objInstance, $boolForceAccess)
                {
                    // Invoke function
                    if($objFunction instanceof ReflectionFunction)
                    {
                        /** @var ReflectionFunction $objFunction */
                        $result = $objFunction->invokeArgs($tabArg);
                    }
                    // Invoke method
                    else
                    {
                        /** @var ReflectionMethod $objFunction */
                        $objFunction->setAccessible($boolForceAccess);
                        $result = $objFunction->invokeArgs($objInstance, $tabArg);
                    }

                    // Return result
                    return $result;
                } :
                null
        );

        // Return result
        return $result;
    }


	
}