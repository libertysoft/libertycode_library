<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\reflection\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\library\file\library\ToolBoxFile;



class ToolBoxFileReflection extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************
	
	/**
	 * Get index array of namespaces,
     * from specified file.
	 *
	 * @param string $strFilePath
	 * @return array
	 */
	public static function getTabNamespace($strFilePath)
	{
		// Init var
		$result = array();

		// Check file path is valid
        if(
            ToolBoxFile::checkFilePathValid($strFilePath) &&
            (trim(strtolower(pathinfo($strFilePath, PATHINFO_EXTENSION))) == 'php')
        )
        {
            // Get index array of namespaces, from file content
            $strContent = file_get_contents($strFilePath);
            $pattern = '#namespace\s+([^\;]+)\;#';
            $tabMatch = array();
            if(
                (preg_match_all($pattern, $strContent, $tabMatch, PREG_PATTERN_ORDER) > 0) &&
                isset($tabMatch[1])
            )
            {
                $result = array_values($tabMatch[1]);
            }
        }
		
		// Return result
		return $result;
	}



    /**
     * Get index array of class paths,
     * from specified file
     * and specified REGEXP pattern.
     *
     * @param string $strFilePath
     * @param string $strPattern
     * @return array
     */
    protected static function getTabClassPathEngine(
        $strFilePath,
        $strPattern
    )
    {
        // Init var
        $result = array();

        // Check file path is valid
        if(
            ToolBoxFile::checkFilePathValid($strFilePath) &&
            (trim(strtolower(pathinfo($strFilePath, PATHINFO_EXTENSION))) == 'php') &&
            is_string($strPattern)
        )
        {
            // Get index array of class paths, from file content
            $strContent = file_get_contents($strFilePath);
            $tabMatch = array();
            if(
                (preg_match_all($strPattern, $strContent, $tabMatch, PREG_PATTERN_ORDER) > 0) &&
                isset($tabMatch[1]) &&
                isset($tabMatch[2]) &&
                (count($tabMatch[1]) == count($tabMatch[2]))
            )
            {
                // Run each namespaces and class names
                $tabNamespace = $tabMatch[1];
                $tabClassNm = $tabMatch[2];
                $strPath = '';
                for($intCpt = 0; ($intCpt < count($tabNamespace)) && ($intCpt < count($tabClassNm)); $intCpt++)
                {
                    // Get namespace
                    $strPath = (
                        (trim($tabNamespace[$intCpt]) != '') ?
                            trim($tabNamespace[$intCpt]) :
                            $strPath
                    );

                    // Get class name
                    $strClassNm = (
                        (trim($tabClassNm[$intCpt]) != '') ?
                            trim($tabClassNm[$intCpt]) :
                            null
                    );

                    // Register class path, if required (class name found)
                    if(!is_null($strClassNm))
                    {
                        $result[] = (
                            (trim($strPath) != '') ?
                                sprintf('%1$s\%2$s', $strPath, $strClassNm):
                                $strClassNm
                        );
                    }
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get index array of class paths,
     * from specified file.
     *
     * @param string $strFilePath
     * @return array
     */
    public static function getTabClassPath($strFilePath)
    {
        // Return result
        return static::getTabClassPathEngine(
            $strFilePath,
            '#namespace\s+([^\;]+)\;|class\s+(\w+)#'
        );
    }



    /**
     * Get index array of interface paths,
     * from specified file.
     *
     * @param string $strFilePath
     * @return array
     */
    public static function getTabInterfacePath($strFilePath)
    {
        // Return result
        return static::getTabClassPathEngine(
            $strFilePath,
            '#namespace\s+([^\;]+)\;|interface\s+(\w+)#'
        );
    }



}