<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\random\library;

use liberty_code\library\instance\model\Multiton;



class ToolBoxRandom extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get random string.
     *
     * @param null|integer $intSize = null : If null, get random integer (between 10-20).
     * @param string $strCharRegexpPattern = '#^[\w-]$#'
     * @return string
     */
    public static function getStrRandom($intSize = null, $strCharRegexpPattern = '#^[\w-]$#')
    {
        // Init var
        $intSize = ((is_int($intSize) && ($intSize > 0)) ? $intSize : rand(10, 20));
        $strCharRegexpPattern = (is_string($strCharRegexpPattern) ? $strCharRegexpPattern : '#^[\w-]$#');
        $result = '';

        // Build random string
        for($intCpt = 0; $intCpt < $intSize; $intCpt++)
        {
            // Get random char
            do {
                $strChar = chr(rand());
            } while(preg_match($strCharRegexpPattern, $strChar) !== 1);

            // Register char
            $result .= $strChar;
        }

        // Return result
        return $result;
    }
	
	
	
}