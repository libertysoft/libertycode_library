<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\library\random\library\ToolBoxRandom;



// Test hash
$intArg1 = 7;
$strArg2 = 'Test class';
$boolArg3 = true;
$tabSize = array(
	-3,
    3,
    10,
    null
);

foreach($tabSize as $intSize)
{
	// Get info
	$strRandom = ToolBoxRandom::getStrRandom($intSize);
	
	echo('Test hash: <br />');
	echo('Size: <pre>');var_dump($intSize);echo('</pre>');
    echo('Random size: <pre>');var_dump(mb_strlen($strRandom));echo('</pre>');
	echo('Random: <pre>');var_dump($strRandom);echo('</pre>');
	
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


