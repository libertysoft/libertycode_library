<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\library\error\library\ToolBoxError;



// Set error handler
/*
set_error_handler(
    function($intErrType, $strErrMsg, $strFilePath, $intFileLine)
    {
        echo('Error": <br />');
        echo('<pre>');
        var_dump(array(
            'type' => $intErrType,
            'msg' => $strErrMsg,
            'file-path' => $strFilePath,
            'file-line' => $intFileLine
        ));
        echo('</pre>');

        echo('<br /><br /><br />');
    }
);
//*/



// Test execute safe
$tabCallable = array(
    [
        function()
        {
            return unserialize(serialize('test'));
        },
        true,
        true
    ], // Ok
    [
        function()
        {
            return unserialize(serialize('test'));
        },
        false,
        false
    ], // Ok
    [
        function()
        {
            return unserialize('test');
        },
        true,
        true
    ], // Ok
    [
        function()
        {
            return unserialize('test');
        },
        false,
        true
    ], // Ko: Throw warning
    [
        function()
        {
            return 1/0;
        },
        true,
        true
    ], // Ok
    [
        function()
        {
            return 1/0;
        },
        false,
        true
    ], // Ok
    [
        function()
        {
            return 1/0;
        },
        false,
        false
    ] // Ko: Throw throwable
);

foreach($tabCallable as $tabInfo)
{
    // Get info
    $callable = $tabInfo[0];
    $boolWithoutErrorRequired = $tabInfo[1];
    $boolWithoutThrowableRequired = $tabInfo[2];

    // Execute
    try
    {
        $result = ToolBoxError::executeSafe($callable, $boolWithoutErrorRequired, $boolWithoutThrowableRequired);

        echo('Test execute safe": <br />');
        echo('<pre>');var_dump($result);echo('</pre>');
    }
    //*
    catch(Throwable $e)
    {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    //*/

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test error handler reset
//unserialize('test');


