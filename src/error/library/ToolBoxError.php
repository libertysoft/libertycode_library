<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\error\library;

use Throwable;
use liberty_code\library\instance\model\Multiton;



class ToolBoxError extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods execute
    // ******************************************************************************

    /**
     * Execute specified callable,
     * without error (except fatal errors), if required,
     * and without throwable, if required.
     *
     * Callable format:
     * mixed function().
     *
     * @param callable $callable
     * @param boolean $boolWithoutErrorRequired = true
     * @param boolean $boolWithoutThrowableRequired = true
     * @return null|mixed
     */
    public static function executeSafe(
        callable $callable,
        $boolWithoutErrorRequired = true,
        $boolWithoutThrowableRequired = true
    )
    {
        // Init var
        $boolWithoutErrorRequired = (is_bool($boolWithoutErrorRequired) ? $boolWithoutErrorRequired : true);
        $boolWithoutThrowableRequired = (is_bool($boolWithoutThrowableRequired) ? $boolWithoutThrowableRequired : true);

        // Disable error/throwable, if required
        $errorReport = 0;
        $displayError = 0;
        if($boolWithoutErrorRequired)
        {
            $errorReport = error_reporting(0);
            $displayError = ini_set('display_errors', 0);
            set_error_handler(function() { return true;});
        }
        if($boolWithoutThrowableRequired)
        {
            set_exception_handler(function() {return true;});
        }

        // Execute callable
        if($boolWithoutThrowableRequired)
        {
            try {$result = $callable();} catch(Throwable $e) {$result = null;}
        }
        else
        {
            $result = $callable();
        }

        // Reset error/throwable, if required
        if($boolWithoutErrorRequired)
        {
            error_reporting($errorReport);
            ini_set('display_errors', $displayError);
            restore_error_handler();
        }
        if($boolWithoutThrowableRequired)
        {
            restore_exception_handler();
        }

        // Return result
        return $result;
    }



}