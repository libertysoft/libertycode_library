<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\datetime\library;

use liberty_code\library\instance\model\Multiton;

use DateTime;
use DateTimeZone;
use DateInterval;
use liberty_code\library\datetime\model\ConfigDateTime;



class ToolBoxDateTime extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ***************************************************************************************************
	
	/**
	 * Get datetime configuration object.
	 * 
	 * @return ConfigDatetime
     */
	public static function getObjConfig()
	{
		// Return result
		return ConfigDatetime::instanceGetDefault();
	}
	
	
	
	
	
	// Methods timezone
	// ***************************************************************************************************
	
	/**
	 * Check if specified timezone exists.
	 * 
	 * @param string $strTz
	 * @return boolean
     */
	public static function checkTzExists($strTz)
	{
		// Return result
		return (
		    is_string($strTz) &&
            in_array($strTz, timezone_identifiers_list())
        );
	}
	
	
	
	/**
	 * Get string default timezone.
	 * 
	 * @return string
     */
	public static function getStrTzDefault()
	{
		// Return result
		return static::getObjConfig()->getStrTimeZoneDefault();
	}
	
	
	
	/**
	 * Get default timezone.
	 * 
	 * @return DateTimeZone
     */
	public static function getObjTzDefault()
	{
		// Return result
		return new DateTimeZone(static::getStrTzDefault());
	}



    /**
     * Get specific timezone.
     *
     * @param null|string $strTz = null: Get default if is null or not exists.
     * @return string
     */
    public static function getObjTz($strTz = null)
    {
        // Return result
        return (
            (static::checkTzExists($strTz)) ?
                new DateTimeZone($strTz) :
                static::getObjTzDefault()
        );
    }





    // Methods format
    // ***************************************************************************************************

    /**
     * Get specified datetime format.
     *
     * @param null|string $strKey = null: Get DB format if is null.
     * @return null|string
     */
    public static function getStrFormat($strKey = null)
    {
        // Init var
        $objConfig = static::getObjConfig();
        $result = (
            is_null($strKey) ?
                $objConfig->getStrFormatDb() :
                (
                    (!is_null($strFormat = $objConfig->getStrFormatDefault($strKey))) ?
                        $strFormat :
                        null
                )
        );


        // Return result
        return $result;
    }





    // Methods datetime
    // ***************************************************************************************************

    /**
     * Check specified string datetime is valid.
     *
     * @param string $strDt
     * @param null|string $strTz = null: Get default if is null.
     * @param null|string $strFormatKeyPattern = null
     * @param boolean $boolFormatDbRequired = false
     * @return boolean
     */
    public static function checkStrDt(
        $strDt,
        $strTz = null,
        $strFormatKeyPattern = null,
        $boolFormatDbRequired = false
    )
    {
        // Return result
        return (!is_null(static::getObjDt($strDt, $strTz, $strFormatKeyPattern, $boolFormatDbRequired)));
    }



    /**
     * Get specified datetime.
     * Use first matched format.
     *
     * @param string $strDt
     * @param string $strTz = null: Get default if is null.
     * @param string $strFormatKeyPattern = null
     * @param boolean $boolFormatDbRequired = false
     * @return null|DateTime
     */
    public static function getObjDt($strDt, $strTz = null, $strFormatKeyPattern = null, $boolFormatDbRequired = false)
    {
        // Init var
        $result = null;

        if(is_string($strDt))
        {
            // Get info
            $objConfig = static::getObjConfig();
            $objTz = static::getObjTz($strTz);
            $boolFormatDbRequired = (is_bool($boolFormatDbRequired) ? $boolFormatDbRequired : false);
            $tabFormat = array_values($objConfig->getTabFormatDefault($strFormatKeyPattern));
            if($boolFormatDbRequired)
            {
                $tabFormat[] = $objConfig->getStrFormatDb();
            }

            // Run each format
            for($intCpt = 0; ($intCpt < count($tabFormat)) && is_null($result); $intCpt++)
            {
                // Get datetime
                $strFormat = $tabFormat[$intCpt];
                $result = (
                    (($objDt = DateTime::createFromFormat($strFormat, $strDt, $objTz)) instanceof DateTime) ?
                        $objDt :
                        null
                );
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get specified string datetime.
     *
     * @param DateTime $objDt
     * @param null|string $strFormatKey = null: Get DB format if is null.
     * @return null|string
     */
    public static function getStrDt(DateTime $objDt, $strFormatKey = null)
    {
        // Return result
        return (
            (!is_null($strFormat = static::getStrFormat($strFormatKey))) ?
                $objDt->format($strFormat) :
                null
        );
    }





	// Methods datetime now
	// ***************************************************************************************************
	
	/**
	 * Get datetime now.
	 *
     * @param null|string $strTz = null: Get default if is null or not exists.
	 * @return DateTime
     */
	public static function getObjDtNow($strTz = null)
	{
		// Return result
		return new DateTime('now', static::getObjTz($strTz));
	}
	
	
	
	/**
	 * Get string DB datetime now.
	 *
     * @param null|string $strTz = null: Get default if is null or not exists.
	 * @return string
     */
	public static function getStrDbDtNow($strTz = null)
	{
		// Return result
		return static::getStrDt(static::getObjDtNow($strTz), null);
	}
	
	
	
	/**
	 * Get string micro-time now.
	 * 
	 * @return string
     */
	public static function getStrTimeNow()
	{
        // Init var
		$intTime = static::getObjDtNow()->getTimestamp();
		$strMicroTime = substr(microtime(), 2, 3); //millieme //substr($MicroTimeVal, 2, 8); // millisecondes
        $result = (strval($intTime) . $strMicroTime);

        // Return result
		return $result;
	}

	



    // Methods operation
    // ***************************************************************************************************

    /**
	 * Get datetime,
     * incremented by specified step,
     * while specified base not over.
	 * By default get datetime with the last day of current month.
	 * 
	 * @param DateTime $objDt
	 * @param string $strBase = 'm': Element of datetime used as base. Can be: 'Y', 'm', 'd', 'H', 'i' or 's'
	 * @param string $strStep = 'P1D': Date interval spec
	 * @param boolean $boolIncrementUp = true: Define the sens of incrementation: true = increment up, false = increment down
	 * @return null|DateTime
     */
    public static function getObjDtLimitStep(
        DateTime $objDt,
        $strBase = 'm',
        $strStep = 'P1D',
        $boolIncrementUp = true
    )
	{
        // Init var
		$result = null;
		
		// Check arguments
		if(($strBase == 'Y') || ($strBase == 'm') || ($strBase == 'd') || ($strBase == 'H') || ($strBase == 'i') || ($strBase == 's'))
		{
			// Init var
			$objDtTarget = clone $objDt;
			$strBaseValue = $objDtTarget->format($strBase);
			
			// Run while base not changed
			while($strBaseValue == $objDtTarget->format($strBase))
			{
				// Increment
				if($boolIncrementUp)
				{
					// Add one day
					$objDtTarget->add(new \DateInterval($strStep));
				}
				else
				{
					// Remove one day
					$objDtTarget->sub(new \DateInterval($strStep));
				}
			}
			
			// Adjust increment
			if($boolIncrementUp)
			{
				// Remove one day
				$objDtTarget->sub(new \DateInterval($strStep));
			}
			else
			{
				// Add one day
				$objDtTarget->Add(new \DateInterval($strStep));
			}
			
			// Set result
			$result = $objDtTarget;
		}
		
		// Return result
		return $result;
	}
	
	
	
	/**
	 * Add specified number of months,
     * in specified datetime.
	 * 
	 * @param DateTime $objDt
     * @param integer $intNbMonth
	 * @return DateTime
     */
	public static function addDtMonthLastDay(DateTime $objDt, $intNbMonth)
	{
        // Init var
        $result = clone $objDt;
		$intCpt = 0;
		$strMonth = $result->format('m');
		
		if($intNbMonth > 0)
		{
            $objDtInterval = new DateInterval("P1D");

			// Run while number months not reached
			while($intCpt <= $intNbMonth)
			{
				// Add one day
                $result->add($objDtInterval);
				
				// Check month reached
				if($result->format('m') != $strMonth)
				{
					$strMonth = $result->format('m');
                    $intCpt++;
				}
			}

            $result->sub($objDtInterval);
		}
		
		// Return result
		return $result;
	}
	
	
	
}