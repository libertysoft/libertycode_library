<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\datetime\library;



class ConstDateTime
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************
	
	// Data constants
	const DATA_KEY_TIMEZONE_DEFAULT = 'strTimeZoneDefault';
	const DATA_KEY_FORMAT_DB = 'strFormatDb';
	const DATA_KEY_FORMAT_LIST_DEFAULT = 'tabFormatDefault';
	
	const DATA_DEFAULT_VALUE_TIMEZONE_DEFAULT = 'UTC';
	const DATE_DEFAULT_VALUE_FORMAT_DB = 'Y-m-d H:i:s';
	const DATE_DEFAULT_VALUE_FORMAT_LIST_DEFAULT = '{"format_long_fr":"d/m/Y H:i:s","format_short_fr":"d/m/Y","format_long_en":"m/d/Y H:i:s","format_short_en":"m/d/Y"}'; // Json format
	
	
	
	// Exception message constants
	const EXCEPT_MSG_TIMEZONE_DEFAULT_INVALID_FORMAT = 'Following timezone "%1$s" invalid! The timezone must be an existing string timezone.';
	const EXCEPT_MSG_FORMAT_DB_INVALID_FORMAT = 'Following format "%1$s" invalid! The format must be a string not empty.';
	const EXCEPT_MSG_FORMAT_LIST_INVALID_FORMAT = 'Following format list "%1$s" invalid! The format list must be an associative array with key and value(format) as string not empty.';



} 