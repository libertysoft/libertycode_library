<?php
/**
 * Description :
 * This class allows to provide configuration for datetime tool.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\datetime\model;

use Exception;
use liberty_code\library\bean\model\FixBean;
use liberty_code\library\datetime\library\ConstDateTime;
use liberty_code\library\datetime\exception\TzDefaultInvalidFormatException;
use liberty_code\library\datetime\exception\FormatDbInvalidFormatException;
use liberty_code\library\datetime\exception\FormatListInvalidFormatException;



/**
 * @method string getStrTimeZoneDefault() Get the string default timezone.
 * @method string getStrFormatDb() Get the string date DB format.
 * method string getTabFormatDefault() Get the associative array of available date formats.
 * @method void setStrTimeZoneDefault(string $strTzDefault) Set the specified default timezone.
 * @method void setStrFormatDb(string $strFormatDb) Set the specified date DB format.
 * @method void setTabFormatDefault(array $tabformat) Set the specified table of format as default.
 */
class ConfigDateTime extends FixBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods initialize
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	protected function beanHydrateDefault()
	{
		if(!$this->beanExists(ConstDateTime::DATA_KEY_TIMEZONE_DEFAULT))
		{
			$this->beanAdd(ConstDateTime::DATA_KEY_TIMEZONE_DEFAULT, ConstDateTime::DATA_DEFAULT_VALUE_TIMEZONE_DEFAULT);
		}
		
		if(!$this->beanExists(ConstDateTime::DATA_KEY_FORMAT_DB))
		{
			$this->beanAdd(ConstDateTime::DATA_KEY_FORMAT_DB, ConstDateTime::DATE_DEFAULT_VALUE_FORMAT_DB);
		}
		
		if(!$this->beanExists(ConstDateTime::DATA_KEY_FORMAT_LIST_DEFAULT))
		{
			$this->beanAdd(ConstDateTime::DATA_KEY_FORMAT_LIST_DEFAULT, json_decode(ConstDateTime::DATE_DEFAULT_VALUE_FORMAT_LIST_DEFAULT, true));
		}
	}





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
			ConstDateTime::DATA_KEY_TIMEZONE_DEFAULT,
			ConstDateTime::DATA_KEY_FORMAT_DB,
			ConstDateTime::DATA_KEY_FORMAT_LIST_DEFAULT
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstDateTime::DATA_KEY_TIMEZONE_DEFAULT:
					TzDefaultInvalidFormatException::setCheck($value);
					break;

				case ConstDateTime::DATA_KEY_FORMAT_DB:
					FormatDbInvalidFormatException::setCheck($value);
					break;

				case ConstDateTime::DATA_KEY_FORMAT_LIST_DEFAULT:
					FormatListInvalidFormatException::setCheck($value);
					break;
			}
		}
		catch(Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}



	
	
	// Methods getters
	// ******************************************************************************
	
	/**
	 * Get default format,
     * from specified key.
	 * 
	 * @param string $strKey
	 * @return null|string
     */
	public function getStrFormatDefault($strKey)
	{
		// Init var
		$tabFormat = $this->beanGet(ConstDateTime::DATA_KEY_FORMAT_LIST_DEFAULT);
        $result = (
            (is_string($strKey) && isset($tabFormat[$strKey])) ?
                $tabFormat[$strKey] :
                null
        );
		
		// Return result
		return $result;
	}



    /**
     * Get associative array of default formats,
     * from specified key REGEXP pattern.
     *
     * @param string $strPattern = null: include all if not provided.
     * @return array
     */
    public function getTabFormatDefault($strPattern = null)
    {
        // Init var
        $strPattern = (is_string($strPattern) ? $strPattern : '#.*#');
        $tabFormat = $this->beanGet(ConstDateTime::DATA_KEY_FORMAT_LIST_DEFAULT);
        $result = array_filter(
            $tabFormat,
            function($strKey) use($strPattern) {return (preg_match($strPattern, $strKey) == 1);},
            ARRAY_FILTER_USE_KEY
        );

        // Return result
        return $result;
    }
	
	
	
}