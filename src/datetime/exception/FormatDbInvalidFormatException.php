<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\datetime\exception;

use Exception;

use liberty_code\library\datetime\library\ConstDateTime;



class FormatDbInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $formatDb
     */
	public function __construct($formatDb) 
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstDateTime::EXCEPT_MSG_FORMAT_DB_INVALID_FORMAT, strval($formatDb));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified date DB format has valid format.
	 * 
     * @param mixed $formatDb
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($formatDb)
    {
		// Init var
		$result = (is_string($formatDb) && (trim($formatDb) != ''));
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($formatDb);
		}
		
		// Return result
		return $result;
    }
	
	
	
}