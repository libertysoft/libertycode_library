<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\datetime\exception;

use Exception;

use liberty_code\library\datetime\library\ConstDateTime;
use liberty_code\library\datetime\library\ToolBoxDateTime;



class TzDefaultInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $tzDefault
     */
	public function __construct($tzDefault) 
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstDateTime::EXCEPT_MSG_TIMEZONE_DEFAULT_INVALID_FORMAT, strval($tzDefault));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified timezone default has valid format.
	 * 
     * @param mixed $tzDefault
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($tzDefault)
    {
		// Init var
		$result = (
		    is_string($tzDefault) &&
            (trim($tzDefault) != '') &&
            ToolBoxDateTime::checkTzExists($tzDefault)
        );
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($tzDefault);
		}
		
		// Return result
		return $result;
    }
	
	
	
}