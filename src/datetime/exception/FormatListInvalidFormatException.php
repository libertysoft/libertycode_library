<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\datetime\exception;

use Exception;

use liberty_code\library\datetime\library\ConstDateTime;



class FormatListInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $tabFormat
     */
	public function __construct($tabFormat) 
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstDateTime::EXCEPT_MSG_FORMAT_LIST_INVALID_FORMAT, strval($tabFormat));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified date list formats has valid format.
	 * 
     * @param mixed $tabFormat
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($tabFormat)
    {
		// Init var
		$result = is_array($tabFormat);
		
		// Check is array
		if($result)
		{
			// Run all elements
			foreach($tabFormat as $key => $value)
			{
				$result = (
                    (is_string($key) && (trim($key) != '')) && // Check key is valid
                    (is_string($value) && (trim($value) != '')) // Check value is valid
                );

				// Break loop if element not valid
				if(!$result)
				{
					break;
				}
			}
		}
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($tabFormat);
		}
		
		// Return result
		return $result;
    }
	
	
	
}