<?php

// Start session
session_start();

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
// use liberty_code\library\datetime\library\ConstDateTime;
// use liberty_code\library\datetime\model\ConfigDateTime;
use liberty_code\library\datetime\library\ToolBoxDateTime;



// Init var, method 1
/*
$tabConfig = array(
	ConstDateTime::DATA_KEY_TIMEZONE_DEFAULT => 'America/New_York',
	ConstDateTime::DATA_KEY_FORMAT_DB => 'test',
	ConstDateTime::DATA_KEY_FORMAT_LIST_DEFAULT => array(
		'e' => 'test'
	)
);
$objConfigDt = ConfigDateTime::instanceGetDefault(); //($tabConfig);
//*/

// Init var, method 2
$objConfigDt = ToolBoxDateTime::getObjConfig();
/*
$objConfigDt->setStrTimeZoneDefault('America/New_York');
$objConfigDt->setStrFormatDb('test');
$objConfigDt->setTabFormatDefault(array(
    'e' => 'test'
));
//*/



// Test config
echo('Test config: <br />');

try{
	$objConfigDt->setStrTimeZoneDefault('test');
} catch (\Exception $e) {
	echo($e->getMessage());echo('<br />');
}

try{
	$objConfigDt->setStrFormatDb(7);
} catch (\Exception $e) {
	echo($e->getMessage());echo('<br />');
}

try{
	$objConfigDt->setTabFormatDefault('test');
} catch (\Exception $e) {
	echo($e->getMessage());echo('<br />');
}

echo('<br /><br /><br />');



// Config: See config
echo('Config: see config : <br />');

$objConfigDt->getStrTimeZoneDefault();
$objConfigDt->getStrFormatDb();
$objConfigDt->getTabFormatDefault();

echo('-> Timezone default: |' . $objConfigDt->getStrTimeZoneDefault() . '|<br />');
echo('-> DB format: |' . $objConfigDt->getStrFormatDb() . '|<br />');
echo('-> List default formats:<br />');
echo('<pre>');var_dump($objConfigDt->getTabFormatDefault());echo('</pre>');
echo('<br />');

$strPatternKey = '#^format_long_.*#';
echo('-> List default formats (pattern: ' . $strPatternKey . '):<br />');
echo('<pre>');var_dump($objConfigDt->getTabFormatDefault($strPatternKey));echo('</pre>');
echo('<br />');

$strPatternKey = '#^format_short_.*#';
echo('-> List default formats (pattern: ' . $strPatternKey . '):<br />');
echo('<pre>');var_dump($objConfigDt->getTabFormatDefault($strPatternKey));echo('</pre>');
echo('<br />');

$strPatternKey = '#.*_fr$#';
echo('-> List default formats (pattern: ' . $strPatternKey . '):<br />');
echo('<pre>');var_dump($objConfigDt->getTabFormatDefault($strPatternKey));echo('</pre>');
echo('<br />');

$strPatternKey = '#.*_en$#';
echo('-> List default formats (pattern: ' . $strPatternKey . '):<br />');
echo('<pre>');var_dump($objConfigDt->getTabFormatDefault($strPatternKey));echo('</pre>');
echo('<br />');

$strPatternKey = '# #';
echo('-> List default formats (pattern: ' . $strPatternKey . '):<br />');
echo('<pre>');var_dump($objConfigDt->getTabFormatDefault($strPatternKey));echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');



// Datetime tool: timezone
echo('Datetime tool: timezone: <br />');
$strTz = 'America/New_York';
echo('-> Timezone exists "' . $strTz . '": |' . ToolBoxDateTime::checkTzExists($strTz) . '|<br />');
$strTz = 'test';
echo('-> Timezone exists "' . $strTz . '": |' . ToolBoxDateTime::checkTzExists($strTz) . '|<br />');

echo('-> Default timezone:<br />');
var_dump(ToolBoxDateTime::getObjTzDefault());
echo('<br />');

echo('-> Timezone:<br />');
var_dump(ToolBoxDateTime::getObjTz('America/New_York'));
echo('<br />');

echo('-> Timezone:<br />');
var_dump(ToolBoxDateTime::getObjTz('test'));
echo('<br />');

echo('-> Timezone:<br />');
var_dump(ToolBoxDateTime::getObjTz());
echo('<br />');

echo('<br /><br /><br />');



// Datetime tool: format
echo('Datetime tool: format: <br />');
echo('-> Format for null key: |' . ToolBoxDateTime::getStrFormat() . '|<br />');
$strFormatKey = 'test';
echo('-> Format for key "' . $strFormatKey . '": |' . ToolBoxDateTime::getStrFormat($strFormatKey) . '|<br />');
$strFormatKey = 'format_long_fr';
echo('-> Format for key "' . $strFormatKey . '": |' . ToolBoxDateTime::getStrFormat($strFormatKey) . '|<br />');
$strFormatKey = 'format_short_en';
echo('-> Format for key "' . $strFormatKey . '": |' . ToolBoxDateTime::getStrFormat($strFormatKey) . '|<br />');

echo('<br /><br /><br />');



// Datetime tool: Datetime
echo('Datetime tool: datetime: <br />');

$strDt = '01/05/2018 09:20:17';
$objDt = ToolBoxDateTime::getObjDt($strDt, 'America/New_York');
echo('-> Datetime for ' . $strDt . ' check: |' . ToolBoxDateTime::checkStrDt($strDt, 'America/New_York') . '|<br />');
echo('-> Datetime for ' . $strDt . ':<br />');
var_dump($objDt);
echo('<br />');

$strDt = 'test';
$objDt = ToolBoxDateTime::getObjDt($strDt);
echo('-> Datetime for ' . $strDt . ' check: |' . ToolBoxDateTime::checkStrDt($strDt) . '|<br />');
echo('-> Datetime for ' . $strDt . ':<br />');
var_dump($objDt);
echo('<br />');

$strDt = '01/05/2018 09:20:17';
$objDt = ToolBoxDateTime::getObjDt($strDt, 'America/New_York', '#.*_fr$#');
$objDt2 = ToolBoxDateTime::getObjDt($strDt, 'America/New_York', '#.*_en$#');
$strFormatKey = 'format_long_fr';
echo('-> Datetime 1 for ' . $strDt . ' and format ' . $strFormatKey . ' check: |' . ToolBoxDateTime::checkStrDt($strDt, 'America/New_York', '#.*_fr$#') . '|<br />');
echo('-> Datetime 1 for ' . $strDt . ' and format ' . $strFormatKey . ': |' . ToolBoxDateTime::getStrDt($objDt, $strFormatKey) . '|<br />');
echo('-> Datetime 2 for ' . $strDt . ' and format ' . $strFormatKey . ': |' . ToolBoxDateTime::getStrDt($objDt2, $strFormatKey) . '|<br />');
$strFormatKey = 'format_short_en';
echo('-> Datetime 1 for ' . $strDt . ' and format ' . $strFormatKey . ' check: |' . ToolBoxDateTime::checkStrDt($strDt, 'America/New_York', '#.*_en$#') . '|<br />');
echo('-> Datetime 1 for ' . $strDt . ' and format ' . $strFormatKey . ': |' . ToolBoxDateTime::getStrDt($objDt, $strFormatKey) . '|<br />');
echo('-> Datetime 2 for ' . $strDt . ' and format ' . $strFormatKey . ': |' . ToolBoxDateTime::getStrDt($objDt2, $strFormatKey) . '|<br />');
echo('<br />');

$strDt = '2018-05-01 09:20:17';
$objDt = ToolBoxDateTime::getObjDt($strDt);
$objDt2 = ToolBoxDateTime::getObjDt($strDt, null, null, true);
$strFormatKey = 'format_long_fr';
echo('-> Datetime 1 for ' . $strDt . ' and format ' . $strFormatKey . ' check: |' . ToolBoxDateTime::checkStrDt($strDt, 'America/New_York', '#.*_fr$#') . '|<br />');
echo('-> Datetime 2 for ' . $strDt . ' and format ' . $strFormatKey . ' check: |' . ToolBoxDateTime::checkStrDt($strDt, 'America/New_York', '#.*_fr$#', true) . '|<br />');
echo('-> Datetime 1 for ' . $strDt . ' and format ' . $strFormatKey . ': |' . (is_null($objDt) ? '-' : ToolBoxDateTime::getStrDt($objDt, $strFormatKey)) . '|<br />');
echo('-> Datetime 2 for ' . $strDt . ' and format ' . $strFormatKey . ': |' . (is_null($objDt) ? '-' : ToolBoxDateTime::getStrDt($objDt2, $strFormatKey)) . '|<br />');
echo('<br />');

echo('<br /><br /><br />');



// Datetime tool: datetime now
echo('Datetime tool: timezone: <br />');

echo('-> Datetime now object:<br />');
var_dump(ToolBoxDateTime::getObjDtNow());
echo('<br />');

$strTz = 'test';
echo('-> Datetime now object (timezone:' . $strTz . '):<br />');
var_dump(ToolBoxDateTime::getObjDtNow($strTz));
echo('<br />');

$strTz = 'America/New_York';
echo('-> Datetime now object (timezone:' . $strTz . '):<br />');
var_dump(ToolBoxDateTime::getObjDtNow($strTz));
echo('<br />');

echo('-> Datetime now DB string: |' . ToolBoxDateTime::getStrDbDtNow() . '|<br />');
$strTz = 'test';
echo('-> Datetime now DB string (timezone:' . $strTz . '): |' . ToolBoxDateTime::getStrDbDtNow($strTz) . '|<br />');
$strTz = 'America/New_York';
echo('-> Datetime now DB string (timezone:' . $strTz . '): |' . ToolBoxDateTime::getStrDbDtNow($strTz) . '|<br />');
echo('-> Time now string: |' . ToolBoxDateTime::getStrTimeNow() . '|<br />');

echo('<br /><br /><br />');



// Datetime tool: operation
echo('Datetime tool: operation: <br />');
$strTz = 'America/New_York';
$objDt = ToolBoxDateTime::getObjDtNow($strTz);
$strFormatKey = 'format_long_fr';
echo('-> Datetime operation last date of month (timezone:' . $strTz . '): |' . ToolBoxDateTime::getStrDt(ToolBoxDateTime::getObjDtLimitStep($objDt), $strFormatKey) . '|<br />');
echo('-> Datetime operation first date of month (timezone:' . $strTz . '): |' . ToolBoxDateTime::getStrDt(ToolBoxDateTime::getObjDtLimitStep($objDt, 'm', 'P1D', false), $strFormatKey) . '|<br />');
echo('-> Datetime operation last month of year (timezone:' . $strTz . '): |' . ToolBoxDateTime::getStrDt(ToolBoxDateTime::getObjDtLimitStep($objDt, 'Y', 'P1M'), $strFormatKey) . '|<br />');
echo('-> Datetime operation last second of day (timezone:' . $strTz . '): |' . ToolBoxDateTime::getStrDt(ToolBoxDateTime::getObjDtLimitStep($objDt, 'd', 'PT1S'), $strFormatKey) . '|<br />');
echo('-> Datetime operation first second of day (timezone:' . $strTz . '): |' . ToolBoxDateTime::getStrDt(ToolBoxDateTime::getObjDtLimitStep($objDt, 'd', 'PT1S', false), $strFormatKey) . '|<br />');

echo('<br /><br /><br />');


