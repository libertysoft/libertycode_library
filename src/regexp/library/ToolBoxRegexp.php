<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\regexp\library;

use liberty_code\library\instance\model\Multiton;



class ToolBoxRegexp extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************
	
	/**
     * Get array of matched values,
     * from specified source,
     * and specified pattern.
     *
	 * @param string $strSrc
     * @param string $strPattern
     * @return array
     */
    public static function getTabMatchValue($strSrc, $strPattern)
    {
        // Init var
        $result = array();
		$tabMatch = array();
		$match = preg_match_all(
            $strPattern,
            $strSrc,
            $tabMatch,
            PREG_SET_ORDER
        );
		
        // Check matches found
        if(($match !== false) && ($match > 0))
        {
            // Run all matches
            foreach($tabMatch as $tabMatchInfo)
            {
                // Run all matched values
                for($intCpt = 1; $intCpt < count($tabMatchInfo); $intCpt++)
                {
                    // Register matched value
                    $result[] = $tabMatchInfo[$intCpt];
                }
            }
        }

        // Return result
        return $result;
    }
	
	
	
}