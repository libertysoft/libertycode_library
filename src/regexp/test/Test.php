<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\library\regexp\library\ToolBoxRegexp;



// Test regexp get matched values
$tabCase = array(
	[
		'/test 1/value 1/test 2/value 2/test 3/value 3/test 4/value 4',
		'#^/test 1/([^/]+)/test 2/(.+)/test 3/(.+)/test 4/([^/]+)$#'
	],
	[
		':test 1:value 1:test 2:value 2:test 3:value 3:test 4:value 4',
		'#\\:([^\\:]+)#'
	],
	[
		':test 1:value 1:test 2:value 2:test 3:value 3:test 4:value 4',
		'#/([^/]+)#'
	]
);

foreach($tabCase as $tabCaseInfo)
{
	// Get info
	$strSrc = $tabCaseInfo[0];
	$strPattern = $tabCaseInfo[1];
	
	echo('Test regexp get matched values: <br />');
	echo('Source: "'.$strSrc.'": <br />');
	echo('Pattern: "'.$strPattern.'": <br />');

	try{
		echo('Get matched values: <pre>');var_dump(ToolBoxRegexp::getTabMatchValue($strSrc, $strPattern));echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


