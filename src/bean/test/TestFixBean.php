<?php

namespace liberty_code\library\bean\test;

use liberty_code\library\bean\model\FixBean;



class TestFixBean extends FixBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************

	public function __construct() 
	{
		parent::__construct();
	}
	
	
	
	
	
	// Methods initialize
	// ******************************************************************************

    /**
     * @inheritdoc
     */
	public function beanHydrateDefault()
	{
		$this->beanHydrate
		(
			array
			(
				'item1' => 'value 1', 
				'item2' => 'value 2', 
				'item3' => 'value 3', 
				'item4' => 'value 4', 
				'item5' => 'value 5'
			)
		);
		//echo($this->getItem1());
	}
	
	
	
	
	
	// Methods setters properties
	// ******************************************************************************
	
	public function setItem1($val)
	{
		//$this->__beanTabData['item1'] = ucfirst($val);
		$this->beanSet('item1', ucfirst($val));
		
		//$this->item1 = ucfirst($val);
		echo('Set Test bean item 1 : |'.$this->__beanTabData['item1'].'|<br />');
	}
	
	public function setItem6($val)
	{
		$this->__beanTabData['item6'] = ucfirst($val);
		echo('Set Test bean item 6 : |'.$this->__beanTabData['item6'].'|<br />');
	}
	
	
	
	
	
	// Methods getters properties
	// ******************************************************************************
	
	public function getItem1()
	{
		//$this->setItem1($this->beanGet('item1'));
		//$this->beanSet('item1', $this->beanGet('item1'));
		return strtoUpper($this->beanGet('item1'));
		//return strtoUpper($this->item1);
	}
	
	public function getItem6()
	{
		return strtoUpper($this->__beanTabData['item6']);
	}
	
	
	
} 