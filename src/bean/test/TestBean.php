<?php

namespace liberty_code\library\bean\test;

use liberty_code\library\bean\model\DefaultBean;



class TestBean extends DefaultBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************

	public function __construct() 
	{
		parent::__construct();
	}
	
	
	
	
	
	// Methods initialize
	// ******************************************************************************

    /**
     * @inheritdoc
     */
	public function beanHydrateDefault()
	{
		$this->beanHydrate
		(
			array
			(
				'item1' => 'value 1', 
				'item2' => 'value 2', 
				'item3' => 'value 3', 
				'item4' => 'value 4', 
				'item5' => 'value 5'
			), 
			true, 
			true
		);
		//echo($this->getItem1());
	}
	
	
	
	
	
	// Methods setters properties
	// ******************************************************************************
	
	public function setItem1($val)
	{
		//$this->__beanTabData['item1'] = ucfirst($val);
		$this->beanSet('item1', ucfirst($val));
		
		//$this->item1 = ucfirst($val);
		echo('Set Test bean item 1 : |'.$this->__beanTabData['item1'].'|<br />');
	}
	
	public function setItem6($val)
	{
		$this->__beanTabData['item6'] = ucfirst($val);
		echo('Set Test bean item 6 : |'.$this->__beanTabData['item6'].'|<br />');
	}
	
	
	
	
	
	// Methods getters properties
	// ******************************************************************************
	
	public function getItem1()
	{
		//$this->setItem1($this->beanGet('item1'));
		//$this->beanSet('item1', $this->beanGet('item1'));
		return strtoUpper($this->beanGet('item1'));
		//return strtoUpper($this->item1);
	}
	
	public function getItem6()
	{
		return strtoUpper($this->__beanTabData['item6']);
	}
	
	
	
	
	
	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$strErrorMsg = null)
	{
		// Init var
		$strErrorMsg = null;
		$result = ($key != 'item8');
		
		if(!$result)
		{
			$strErrorMsg = 'Impossible to use key "' . $key . '".';
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$strErrorMsg = null)
	{
		// Init var
		$strErrorMsg = null;
		$result = 
			($key != 'item7') || 
			(($key == 'item7') && (is_int($value)));
		
		if(!$result)
		{
			$strErrorMsg = 'The value must be an integer.';
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$strErrorMsg = null)
	{
		// Init var
		$strErrorMsg = null;
		$result = ($key != 'item7');

		if(!$result)
		{
			$strErrorMsg = 'Impossible to remove key "' . $key . '".';
		}

		// Return result
		return $result;
	}
	
	


	
	// Methods events
	// ******************************************************************************
	
	
	
	protected function printEvent($strEvent, $strKey, $value = null)
	{
		echo('<br />');
		var_dump(sprintf('Test event "%1$s" for key "%2$s".', $strEvent, $strKey));
		echo('<br />');
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	protected function beanOnBeforeAddValue($key, $value)
	{
		$this->printEvent('Before add', $key, $value);
	}



	/**
	 * @inheritdoc
	 */
	protected function beanOnAfterAddValue($key)
	{
		$this->printEvent('After add', $key);
	}



	/**
	 * @inheritdoc
	 */
	protected function beanOnBeforeSetValue($key, $value)
	{
		$this->printEvent('Before set', $key, $value);
	}



	/**
	 * @inheritdoc
	 */
	protected function beanOnAfterSetValue($key)
	{
		$this->printEvent('After set', $key);
	}



	/**
	 * @inheritdoc
	 */
	protected function beanOnBeforeRemoveValue($key)
	{
		$this->printEvent('Before remove', $key);
	}



	/**
	 * @inheritdoc
	 */
	protected function beanOnAfterRemoveValue($key)
	{
		$this->printEvent('After remove', $key);
	}
	
	
	
} 