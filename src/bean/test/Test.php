<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
include($strRootAppPath . '/src/bean/test/TestBean.php');
include($strRootAppPath . '/src/bean/test/TestFixBean.php');

// Use
use liberty_code\library\bean\library\ConstBean;
use liberty_code\library\bean\test\TestBean;
use liberty_code\library\bean\test\TestFixBean;

//echo('|'.get_class().'|'.get_called_class().'|');



// Test
$objTestBean = new TestBean();
echo('Test Get Test bean : <br />');echo($objTestBean[0]);
echo('Get Test bean item 1 : |'.$objTestBean->getItem1().'|'.$objTestBean->beanGetData('item1').'|'.$objTestBean['item1'].'|'.$objTestBean[0].'|<br />');
echo('Get Test bean item 2 : |'.$objTestBean->item2.'|<br />');
echo('Get Test bean item 3 : |'.$objTestBean->getItem3().'|<br />');
echo('Get Test bean item 4 : |'.$objTestBean->item4.'|<br />');
echo('Get Test bean item 5 : |'.$objTestBean->item5.'|<br />');
echo('<br /><br /><br />');



echo('Test Get Test bean after set : <br />');
$objTestBean->item1 = 'test value 1';
$objTestBean->item2 = 'Test Value 2';
echo('Get Test bean item 1 : |'.$objTestBean->item1.'|<br />');
echo('Get Test bean item 2 : |'.$objTestBean->item2.'|<br />');
echo('<br /><br /><br />');

echo('Test print bean : <br />');
echo($objTestBean);
echo('<br /><br /><br />');

echo('Test call bean : <br />');
echo('Get Test bean item 1 : |'.$objTestBean('item1').'|<br />');
try
{
	echo('Get Test bean item _1 : |'.$objTestBean('item_1').'|<br />');
}
catch(Exception $e)
{
	echo($e->getMessage());
}
echo('<br /><br /><br />');


echo('Test clone bean : <br />');
$objTestBean2 = clone $objTestBean;
echo('Get Test bean 2 item 1 : |'.$objTestBean2->item1.'|<br />');
echo('Get Test bean 2 item 2 : |'.$objTestBean2->item2.'|<br />');
echo('<br /><br /><br />');


echo('Test Arrayable bean : <br />');
$cpt = 0;
while($cpt < Count($objTestBean2))
{
	$strKey = $objTestBean2->beanGetStrKeyFromIndex($cpt);
	echo('Get Test bean 2 / index |'.$cpt.'| / key :|'.$strKey.'| / value by index :|'.$objTestBean2[$cpt].'| / value by key :|'.$objTestBean2[$strKey].'|<br />');
	
	$cpt++;
}
echo('<br /><br /><br />');


echo('Test Iteratable bean : <br />');
foreach($objTestBean2 as $key => $value)
{
	echo('Get Test bean 2 / key :|'.$key.'| / value :|'.$value.'|<br />');
}
echo('<br /><br /><br />');



echo('Test collection bean : <br />');
$objTestBean2[3] = 'valUE 4';
//$objTestBean2[5] = 'valUE 66';
$objTestBean2['item6'] = 'valUE 6';
$objTestBean2[5] = 'valUE 66';
if(isset($objTestBean2->item6))
{
	echo('Test isset item 6 created<br />');
}
else
{
	if(isset($objTestBean2->item6))
	{
		echo('Test isset item 6 created<br />');
	}
	else
	{
		echo('Test isset item 6 not created<br />');
	}
	
	// Create
	$objTestBean2->item6 = 'value 6';
	echo('Test isset item 6 created<br />');
}
if($objTestBean2->existsItem6())
{
	echo('Test isset item 6 ok<br />');
}
if(isset($objTestBean2['item6']))
{
	echo('Test isset item 6 ok<br />');
}
else
{
	echo('Test isset item 6 ko<br />');
}
$objTestBean2->setItem6('value 6666');
$objTestBean2['item6'] = 'value 66667';
try
{
	$objTestBean2->beanPutData('item6', 'value 6');
}
catch(Exception $e)
{
	echo($e->getMessage().'<br />');
}

echo('<br /><br /><br />');



foreach($objTestBean2->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY_VALUE) as $key => $value)
{
	echo('Get Test bean 2 / key :|'.$key.'| / value :|'.$value.'|<br />');
}

//$objTestBean2->remove('item6');
//unset($objTestBean2->item6);
//$objTestBean2->removeItem6();
unset($objTestBean2['item6']);
try
{
	$objTestBean2->beanRemoveData('item6');
}
catch(Exception $e)
{
	echo($e->getMessage().'<br />');
}

echo('<br /><br /><br />');



foreach($objTestBean2->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY) as $key)
{
	$strMethodGet = 'get'.ucfirst($key);
	echo('Get Test bean 2 / key :|'.$key.'|'.$objTestBean2->$strMethodGet().'|<br />');
}

echo('<br />');

foreach($objTestBean2->beanGetTabData(ConstBean::OPTION_TABLE_DATA_VALUE) as $key => $value)
{
	echo('Get Test bean 2 / value :|'.$value.'|<br />');
}

echo('<br />');

foreach($objTestBean2->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY_VALUE) as $key => $value)
{
	echo('Get Test bean 2 / key :|'.$key.'| / value :|'.$value.'|<br />');
}

echo('<br /><br /><br />');



echo('Index test bean:'.TestBean::instanceGetIntIndex($objTestBean).' - ');
echo('Index test bean 2:'.TestBean::instanceGetIntIndex($objTestBean2).'<br />');
TestBean::instanceGet(0)->item2 = 'val 2';
TestBean::instanceGet(1)->item2 = 'val 2222';
echo('Test bean, item 2:'.$objTestBean->item2.' - ');
echo('Test bean 2, item 2:'.$objTestBean2->item2.'<br />');

echo('<br /><br /><br />');



// Test fix bean
$objTestFixBean = new TestFixBean();
foreach($objTestFixBean as $key => $value)
{
	echo('Get Test fix bean / key :|'.$key.'| / value :|'.$value.'|<br />');
}

echo('<br /><br /><br />');



try
{
	//$objTestFixBean->beanAddData('item6', 'value 6');
	$objTestFixBean['item6'] = 'value 6';
}
catch(Exception $e)
{
	echo($e->getMessage().'<br />');
}

try
{
	//$objTestFixBean->beanRemoveData('item1');
	unset($objTestFixBean['item1']);
}
catch(Exception $e)
{
	echo($e->getMessage().'<br />');
}

echo('<br /><br /><br />');



foreach($objTestFixBean as $key => $value)
{
	echo('Get Test fix bean / key :|'.$key.'| / value :|'.$value.'|<br />');
}

echo('<br /><br /><br />');



// Test handle bean
$objTestHandleBean = new TestBean();

echo('Test handle bean:<br />');



try
{
	$objTestHandleBean->item8 = 'value 8';
}
catch(Exception $e)
{
	echo($e->getMessage().'<br />');
}

echo('<br /><br /><br />');



try
{
	$objTestHandleBean->item7 = 'value 7';
}
catch(Exception $e)
{
	echo($e->getMessage().'<br />');
}

$objTestHandleBean->item7 = 7;
echo('Test handle bean, item 7:'.$objTestHandleBean->item7.'<br />');
$objTestHandleBean->item7 = 8;
echo('Test handle bean, item 7:'.$objTestHandleBean->item7.'<br />');

echo('<br /><br /><br />');



try
{
	//$objTestHandleBean->beanRemoveData('item7');
	unset($objTestHandleBean['item7']);
}
catch(Exception $e)
{
	echo($e->getMessage().'<br />');
}

echo('<br /><br /><br />');


