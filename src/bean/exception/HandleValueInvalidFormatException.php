<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\bean\exception;

use Exception;

use liberty_code\library\bean\library\ConstBean;



class HandleValueInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $path
	 * @param mixed $value
	 * @param mixed $errorMsg
     */
	public function __construct($path, $value, $errorMsg)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$strErrorMsg =
			(
				(!is_null($errorMsg)) &&
				is_string($errorMsg) && (trim($errorMsg) != '')
			) ?
				' ' . trim($errorMsg) :
				'';
		$this->message = sprintf(ConstBean::EXCEPT_MSG_HANDLE_VALUE_INVALID_FORMAT, strval($path), strval($value), $strErrorMsg);
	}
	
	
	
}