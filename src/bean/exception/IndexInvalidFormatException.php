<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\bean\exception;

use Exception;

use liberty_code\library\bean\library\ConstBean;



class IndexInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $index
     */
	public function __construct($index) 
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstBean::EXCEPT_MSG_INDEX_INVALID_FORMAT, strval($index));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified index has valid format.
	 * 
     * @param mixed $index
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($index)
    {
		// Init var
		$result = (is_int($index) && ($index >= 0));
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($index);
		}
		
		// Return result
		return $result;
    }
	
	
	
}