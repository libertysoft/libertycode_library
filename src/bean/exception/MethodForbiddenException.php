<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\bean\exception;

use Exception;

use liberty_code\library\bean\library\ConstBean;



class MethodForbiddenException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param string $strClassNm: class name
	 * @param string $strMethodNm: Method name
     */
	public function __construct($strClassNm, $strMethodNm) 
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstBean::EXCEPT_MSG_METHOD_FORBIDDEN, strval($strClassNm), strval($strMethodNm));
	}
	
	
	
}