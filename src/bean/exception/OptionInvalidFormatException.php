<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\bean\exception;

use Exception;

use liberty_code\library\bean\library\ConstBean;



class OptionInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $option
     */
	public function __construct($option) 
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstBean::EXCEPT_MSG_OPTION_INVALID_FORMAT, strval($option));
	}
	
	
	
}