<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\bean\exception;

use Exception;

use liberty_code\library\bean\library\ConstBean;



class KeyIndexInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $key
     */
	public function __construct($key) 
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstBean::EXCEPT_MSG_KEY_INDEX_INVALID_FORMAT, strval($key));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified index has valid format.
	 * 
     * @param mixed $key
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($key)
    {
		// Init var
		$result = (is_int($key) || is_string($key));
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($key);
		}
		
		// Return result
		return $result;
    }
	
	
	
}