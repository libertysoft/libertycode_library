<?php
/**
 * Short description :
 * Regroup all constants about beans.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\bean\library;



class ConstBean
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************
	
	// Configuration
	const PARAM_KEY_STRING_ALLOWED = ['_'];
	
	const OPTION_TABLE_DATA_KEY = 'opt_table_data_key';
	const OPTION_TABLE_DATA_VALUE = 'opt_table_data_value';
	const OPTION_TABLE_DATA_KEY_VALUE = 'opt_table_data_key_value';
	
	
	
	// Exception message constants
	const EXCEPT_MSG_KEY_INVALID_FORMAT = 'Following key "%1s" invalid! The key must be an alphanum string, not empty and not start with number.';
	const EXCEPT_MSG_KEY_FOUND = 'Following key "%1s" already exists!';
	const EXCEPT_MSG_KEY_NOT_FOUND = 'Following key "%1s" not found!';
	const EXCEPT_MSG_INDEX_INVALID_FORMAT = 'Following index "%1s" invalid! The index must be a positive integer.';
	const EXCEPT_MSG_INDEX_NOT_FOUND = 'Following index "%1s" out of bound!';
	const EXCEPT_MSG_KEY_INDEX_INVALID_FORMAT = 'Following key "%1s" invalid! The key must be an integer or a string.';
	const EXCEPT_MSG_OPTION_INVALID_FORMAT = 'Following option "%1s" invalid!';
	const EXCEPT_MSG_TABLE_DATA_INVALID_FORMAT = 'Data table provided invalid! The data table must be a valid associative table with all keys as alphanum string, not empty, not start with number and unique.';
	const EXCEPT_MSG_METHOD_FORBIDDEN = 'Method forbidden! The method "%2$s" in class "%1$s" is forbidden.';
	const EXCEPT_MSG_HANDLE_KEY_INVALID_FORMAT = 'Following key "%1$s" failed validation handle rule!%2$s';
	const EXCEPT_MSG_HANDLE_VALUE_INVALID_FORMAT = 'Following value "%2$s" (for key "%1$s") failed validation handle rule!%3$s';
	const EXCEPT_MSG_HANDLE_KEY_NOT_REMOVE = 'Following key "%1$s" failed validation handle rule to remove it!%2$s';



} 