<?php
/**
 * Description :
 * This class allows to define bean class.
 * Bean allows to handle properties.
 * Can be consider is base of all bean types.
 *
 * Note:
 * -> Property:
 *     Represents specific protected data, can be stored in bean, like key/value pair.
 * -> Property key format:
 *     Must be an alphanumeric string, not empty and not start with number.
 *     It follows same rules like variable name.
 * -> Property value format:
 *     Can be any type.
 *
 * Feature:
 * -> Initialize properties :
 *     Use @see beanHydrate() .
 * -> Check property exists:
 *     - @see beanDataExists() .
 *     - isset($this->${'Property key'}), example : isset($this->propertyKey)
 *     - $this->${'exists'.ucfirst('Property key')}(), example : $this->existsPropertyKey()
 *     => Return true if exists, false else.
 * -> Get property value:
 *     - @see beanGetData() .
 *     - $this->${'Property key'}, example : $this->propertyKey
 *     - $this->${'get'.ucfirst('Property key')}(), example : $this->getPropertyKey()
 *     - $this('Property key')
 *     => Search getter function if it's defined, else search directly from properties.
 * -> Set property:
 *     - @see beanSetData() .
 *     - $this->${'set'.ucfirst('Property key')}(Value), example : $this->setPropertyKey(Value)
 *     => Search setter function if it's defined, else set directly on properties.
 * -> Add property:
 *     - @see beanAddData() .
 *     - $this->${'add'.ucfirst('Property key')}(Value), example : $this->addPropertyKey(Value)
 *     => Search setter function if it's defined, else set directly on properties.
 * -> Put property (set if exists, add else):
 *     - @see beanPutData() .
 *     - $this->${'Property key'} = Value, example : $this->propertyKey = Value
 *     - $this->${'put'.ucfirst('Property key')}(Value), example : $this->putPropertyKey(Value)
 *     => Search setter function if it's defined, else set directly on properties.
 * -> Remove property:
 *     - @see beanRemoveData() .
 *     - unset($this->${'Property key'}), example : unset($this->propertyKey)
 *     - $this->${'remove'.ucfirst('Property key')}(), example : $this->removePropertyKey()
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\bean\model;

use liberty_code\library\instance\model\Multiton;

use Exception;
use BadFunctionCallException;
use ReflectionMethod;
use liberty_code\library\bean\library\ConstBean;
use liberty_code\library\bean\exception\KeyInvalidFormatException;
use liberty_code\library\bean\exception\KeyFoundException;
use liberty_code\library\bean\exception\KeyNotFoundException;
use liberty_code\library\bean\exception\OptionInvalidFormatException;



abstract class Bean extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * Array of cached method allowed checks.
     * Format: Method name => boolean (is allowed)
     * @var array
     */
    static protected $__beanTabCacheMethodAllowed = array();


	
	/**
	 * Data array of properties.
     * Format: String property key => mixed property value
     * @var array
     */
	protected $__beanTabData;



    /**
     * Check if initialized.
     * @var boolean
     */
    protected $__beanBoolInit;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************

	/**
     * @inheritdoc
	 * @param array $tabData = array()
     */
	public function __construct(array $tabData = array())
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->__beanTabData = array();
        $this->__beanBoolInit = false;
		
		// Hydrate data
		$this->beanHydrate(
		    $tabData,
            true,
            true,
            true
        );
	}
	
	
	
	/**
	 * @inheritdoc
     */
	public function __clone()
	{
		// Call parent clone
		parent::__clone();
	}
	
	
	
	
	
	// Magical methods
	// ******************************************************************************
	
	/**
	 * Magical method allows to get string, from instance.
	 * It return serialization of data array.
	 * 
	 * @return string
     */
	public function __toString()
	{
		return serialize($this->__beanTabData);
	}
	
	
	
	/**
	 * Magical method allows to catch call of class, as function.
     * Get the value from specified property.
	 * 
	 * @param $key
	 * @return object
     */
	public function __invoke($key)
	{
		return $this->beanGetData($key);
	}
	
	
	
	/**
	 * Magical method allows to catch call of checking existence of specified property.
	 * 
	 * @param $key
	 * @return boolean
     */
	public function __isset($key)
	{
		return $this->beanDataExists($key);
	}
	
	
	
	/**
	 * Magical method allows to catch call of removing specified property.
	 * 
	 * @param $key
     */
	public function __unset($key)
	{
		$this->beanRemoveData($key);
	}
	
	
	
	/**
	 * Magical method allows to catch call of setting specified property.
	 * 
	 * @param $key
	 * @param $value
     */
	public function __set($key, $value)
	{
		// Check data exists
		$this->beanPutData($key, $value);
	}
	
	
	
	/**
	 * Magical method allows to catch call of getting specified property.
	 * 
	 * @param $key
	 * @return object
     */
	public function __get($key)
	{
		return $this->beanGetData($key);
	}
	
	
	
	/**
	 * Magical method allows to catch call of methods.
	 * 
	 * @param $strMethodNm
	 * @param $tabMethodArg
	 * @return object
     */
	public function __call($strMethodNm, $tabMethodArg)
	{
        // Init var
        $result = null;
        $intMethodArgCount = count($tabMethodArg);
        $tabMethodConfig = array
        (
            array('exists', 0),
            array('get', 0),
            array('set', 1),
            array('add', 1),
            array('put', 1),
            array('remove', 0)
        );

        // Run each method configuration
        for($intCpt = 0, $boolFind = false; ($intCpt < count($tabMethodConfig)) && (!$boolFind); $intCpt++)
        {
            // Init var
            $strExpectedMethodStart = $tabMethodConfig[$intCpt][0];
            $intExpectedMethodArgCount = $tabMethodConfig[$intCpt][1];
            $strKey = '';
            $boolFind = (
                ($strExpectedMethodStart === substr($strMethodNm, 0, ($intExpectedMethodStart = strlen($strExpectedMethodStart)))) &&
                (($strKey = lcfirst(substr($strMethodNm, $intExpectedMethodStart))) !== '') &&
                ($intExpectedMethodArgCount === $intMethodArgCount)
            );

            // Execute method, if required (found)
            if($boolFind)
            {
                switch($strExpectedMethodStart)
                {
                    case 'exists':
                        $result = $this->beanDataExists($strKey);
                        break;

                    case 'get':
                        $result = $this->beanGetData($strKey);
                        break;

                    case 'set':
                        $this->beanSetData($strKey, $tabMethodArg[0]);
                        break;

                    case 'add':
                        $this->beanAddData($strKey, $tabMethodArg[0]);
                        break;

                    case 'put':
                        $this->beanPutData($strKey, $tabMethodArg[0]);
                        break;

                    case 'remove':
                        $this->beanRemoveData($strKey);
                        break;
                }
            }
        }

        // Check method found
        if(!$boolFind)
        {
            throw new BadFunctionCallException(sprintf(
                'Method failed! No method \'%s\' with %d argument(s) found.',
                $strMethodNm,
                $intMethodArgCount
            ));
        }

        // Return result
        return $result;
	}
	
	
	
	
	
	// Methods initialize
	// ******************************************************************************
	
	/**
	 * Hydrate properties,
     * from specified data.
     *
	 * Data array format:
	 * [
	 * 		'String property 1 key': mixed property 1 value,
	 * 		...,
	 * 		'String property N key': mixed property N value,
	 * ]
	 * 
	 * @param array $tabData = array()
	 * @param boolean $boolTranslate = true
	 * @param boolean $boolPublic = false
     * @param boolean $boolClean = true
	 * @return boolean
     */
	protected function beanHydrate(
	    array $tabData = array(),
        $boolTranslate = true,
        $boolPublic = false,
        $boolClean = true
    )
	{
		// Init var
		$result = false;
        $boolPublic = (is_bool($boolPublic) ? $boolPublic : false);
        $boolClean = (is_bool($boolClean) ? $boolClean : false);

        // Clean, if required
        if($boolClean)
        {
            $this->__beanTabData = array();
        }

		// Run each data
		foreach($tabData as $key => $value)
		{
			$result = true;

			// Set data
			if($boolPublic)
			{
				$this->beanPutData($key, $value);
			}
			else
			{
				$this->beanPut($key, $value, $boolTranslate);
			}
		}

        $this->__beanBoolInit = true;
		
		// Return result
		return $result;
	}
	
	
	
	

    // Methods validation
    // ******************************************************************************

    /**
     * Set property key validation check,
     * from specified property key.
     * Overwrite it to set specific rules.
     *
     * @param string $key
     * @throws Exception
     * @throws KeyInvalidFormatException
     */
    protected function beanSetValidKey($key)
    {
        // Set check format key
        KeyInvalidFormatException::setCheck($key);
    }



    /**
     * Set property value validation check,
     * from specified property key,
     * and specified property value.
     * Overwrite it to set specific rules.
     *
     * @param string $key
     * @param mixed $value
     * @throws Exception
     */
    protected function beanSetValidValue($key, $value)
    {
        // Do nothing
    }



    /**
     * Set property is removable validation,
     * from specified property key.
     * Overwrite it to set specific rules.
     *
     * @param string $key
     * @throws Exception
     */
    protected function beanSetValidRemove($key)
    {
        // Do nothing
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if specified property exists engine.
     *
     * @param string $key
     * @return boolean
     */
    protected function beanExistsEngine($key)
    {
        // Return result
        return (
            isset($this->__beanTabData[$key]) ||  // Check via isset first (more fast)
            array_key_exists($key, $this->__beanTabData)
        );
    }



	/**
	 * Check if specified property exists.
     * Main internal method.
	 * 
	 * @param string $key
	 * @return boolean
	 */
	protected function beanExists($key)
	{
		// Set check property key
        $this->beanSetValidKey($key);

		// Return result
		return $this->beanExistsEngine($key);
	}
	
	
	
	/**
	 * Check if the specified method is allowed,
     * to be executed.
	 * 
	 * @param string $strMethod
	 * @return boolean
     */
	protected function beanMethodAllowed($strMethod)
	{
		// Init var
        $checkMethodAllowed = function() use ($strMethod)
        {
            return (
                // Check method exists
                (method_exists($this, $strMethod)) &&

                // Check method accessible:
                // Only Public authorized.
                // Private forbidden access in extern and in intern. If its call in correct class, the good method call.
                // Protected forbidden access in extern but authorized in intern. If its call in correct class or child class, the good method call.
                (new ReflectionMethod(get_called_class(), $strMethod))->isPublic()
            );
		};
        $result = (
            is_string($strMethod) ?
                (static::$__beanTabCacheMethodAllowed[$strMethod] ?? $checkMethodAllowed()) :
                false
        );
        static::$__beanTabCacheMethodAllowed[$strMethod] = $result;
		
		// Return result
		return $result;
	}





    // Methods getters
    // ******************************************************************************

    /**
     * Get specified property engine.
     *
     * @param string $key
     * @return mixed
     */
    protected function beanGetEngine($key)
    {
        // Return result
        return $this->__beanTabData[$key];
    }



    /**
     * Get specified property.
     * Main internal method.
     *
     * If property exists in data array:
     * -> Try to call get.ucfirst('property key') method, if exists.
     * -> If method get not exists, return value, from data array.
     * If property not exists in the data array, throw error.
     *
     * @param string $key
     * @param boolean $boolTranslate = true
     * @return mixed
     * @throws KeyNotFoundException
     */
    protected function beanGet($key, $boolTranslate = false)
    {
        // Set check property exists
        if(!$this->beanExists($key))
        {
            throw new KeyNotFoundException($key);
        }

        // Init var
        $strMethod = 'get'.ucfirst($key);
        $result = (
            ($boolTranslate && $this->beanMethodAllowed($strMethod)) ?
                $this->$strMethod() : // Get property with method
                $this->beanGetEngine($key) // Get property without method
        );

        // Return result
        return $result;
    }





	// Methods setters
	// ******************************************************************************

	/**
	 * Set specified property engine.
	 *
	 * @param string $key
	 * @param mixed $value
	 */
	protected function beanSetEngine($key, $value)
	{
		// Set property
		$this->__beanTabData[$key] = $value;
	}



	/**
     * Set specified property.
     * If not exists in data array, throw error.
     * Main internal method.
	 * 
	 * @param string $key
	 * @param mixed $value
	 * @param boolean $boolTranslate = true
	 * @throws KeyNotFoundException
     */
	protected function beanSet($key, $value, $boolTranslate = false)
	{
        // Set check property key, if required
        if(!$this->__beanBoolInit)
        {
            $this->beanSetValidKey($key);
        }
        // Set check property exists, if required
        else if(!$this->beanExists($key))
        {
            throw new KeyNotFoundException($key);
        }

        // Set check property value
        $this->beanSetValidValue($key, $value);

        // Init var
        $strMethod = 'set'.ucfirst($key);

        // Check method exists
        if($boolTranslate && $this->beanMethodAllowed($strMethod))
        {
            // Set property with method
            $this->$strMethod($value);
        }
        else
        {
            // Set property without method
            $this->beanSetEngine($key, $value);
        }
	}
	
	
	
	/**
     * Add specified property.
     * If exists in data array, throw error.
     * Main internal method.
     *
	 * @param string $key
	 * @param mixed $value
	 * @param boolean $boolTranslate = true
	 * @throws KeyFoundException
     */
	protected function beanAdd($key, $value, $boolTranslate = false)
	{
        // Set check property not exists
        if($this->beanExists($key))
        {
            throw new KeyFoundException($key);
        }

        // Set property
        $this->__beanBoolInit = false;
        $this->beanSet($key, $value, $boolTranslate);
        $this->__beanBoolInit = true;
	}
	
	
	
	/**
     * Put specified property:
     * Set if exists, add else.
     * Main internal method.
	 * 
	 * @param string $key
	 * @param mixed $value
	 * @param boolean $boolTranslate = true
     */
	protected function beanPut($key, $value, $boolTranslate = false)
	{
        // Set check property key
        $this->beanSetValidKey($key);

        // Set property
        $this->__beanBoolInit = false;
        $this->beanSet($key, $value, $boolTranslate);
        $this->__beanBoolInit = true;
	}



    /**
     * Remove specified property engine.
     *
     * @param string $key
     */
    protected function beanRemoveEngine($key)
    {
        unset($this->__beanTabData[$key]);
    }



	/**
     * Remove specified property.
     * Main internal method.
	 * 
	 * @param string $key
	 * @throws KeyNotFoundException
     */
	protected function beanRemove($key)
	{
        // Set check property exists
        if(!$this->beanExists($key))
        {
            throw new KeyNotFoundException($key);
        }

        // Set check property removable
        $this->beanSetValidRemove($key);

        // Remove property
        $this->beanRemoveEngine($key);
	}





    // Methods public interfaces
    // ******************************************************************************

    /**
     * Check if specified property exists.
     * Main public method.
     *
     * @param string $key
     * @return boolean
     */
    public function beanDataExists($key)
    {
        // Return result
        return $this->beanExists($key);
    }



    /**
     * Get specified property.
     * Main public method.
     *
     * @param string $key
     * @return mixed
     */
    public function beanGetData($key)
    {
        // Return result
        return $this->beanGet($key, true);
    }



	/**
     * Get properties array.
     *
     * Option format:
	 * - ConstBean::OPTION_TABLE_DATA_KEY:
     *     Return index array with all properties keys.
	 * - ConstBean::OPTION_TABLE_DATA_VALUE:
     *     Return index array with all properties values.
	 * - ConstBean::OPTION_TABLE_DATA_KEY_VALUE:
     *     Return associative array like: [property key => property value].
	 * 
	 * @param string $strOption = ConstBean::OPTION_TABLE_DATA_KEY
	 * @return array
	 * @throws OptionInvalidFormatException
     */
	public function beanGetTabData($strOption = ConstBean::OPTION_TABLE_DATA_KEY)
	{
		// Init result
		//$result = array();

        // Build array, from option
        switch($strOption)
        {
            case ConstBean::OPTION_TABLE_DATA_KEY:
                $result = array_keys($this->__beanTabData);
                break;

            case ConstBean::OPTION_TABLE_DATA_VALUE:
            case ConstBean::OPTION_TABLE_DATA_KEY_VALUE:
                $result = $this->__beanTabData;
                array_walk(
                    $result,
                    function(&$item1, $key)
                    {
                        return $this->beanGet($key, true);
                    }
                );

                if($strOption == ConstBean::OPTION_TABLE_DATA_VALUE)
                {
                    $result = array_values($result);
                }

                break;

            default:
                throw new OptionInvalidFormatException($strOption);
                break;
        }
		
		// Return result
		return $result;
	}
	
	
	
	/**
     * Set specified property.
     * Main public method.
	 * 
	 * @param string $key
	 * @param mixed $value
	 */
	public function beanSetData($key, $value)
	{
		$this->beanSet($key, $value, true);
	}
	
	
	
	/**
     * Add specified property.
     * Main public method.
	 * 
	 * @param string $key
	 * @param mixed $value
	 */
	public function beanAddData($key, $value)
	{
		$this->beanAdd($key, $value, true);
	}
	
	
	
	/**
     * Put specified property.
     * Main public method.
	 * 
	 * @param string $key
	 * @param mixed $value
	 */
	public function beanPutData($key, $value)
	{
        $this->beanPut($key, $value, true);
	}
	
	
	
	/**
     * Remove specified property.
     * Main public method.
	 * 
	 * @param string $key
	 */
	public function beanRemoveData($key)
	{
		$this->beanRemove($key);
	}



    /**
     * Remove bean cache.
     */
    public function beanRemoveCache()
    {
        static::$__beanTabCacheMethodAllowed = array();
    }


	
} 