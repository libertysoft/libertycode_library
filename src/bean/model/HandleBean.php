<?php
/**
 * Description :
 * This class allows to define handle bean class.
 * Handle bean is bean, allows to handle property control.
 * Can be consider is base of all handle bean types.
 *
 * Feature:
 * -> Check property keys/values.
 * -> Set events on properties actions (Add, set (update) and remove).
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\bean\model;

use liberty_code\library\bean\model\Bean;

use Exception;
use liberty_code\library\bean\exception\HandleKeyInvalidFormatException;
use liberty_code\library\bean\exception\HandleValueInvalidFormatException;
use liberty_code\library\bean\exception\HandleKeyNotRemoveException;



abstract class HandleBean extends Bean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * Array of cached property key valid checks.
     * Format: Property key => [boolean is valid, string|Exception error]
     * @var array
     */
    static protected $__beanTabCacheValidKey = array();



    /**
     * Array of cached property removable checks.
     * Format: Property key => [boolean is valid, string|Exception error]
     * @var array
     */
    static protected $__beanTabCacheValidRemove = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods validation
	// ******************************************************************************

	/**
	 * Check if specified property key is valid.
	 * Overwrite it to set specific rules.
	 *
	 * @param string $key
	 * @param string|Exception &$error = null
	 * @return boolean
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Return result
		return true;
	}



	/**
	 * Check if specified property value is valid,
     * from specified property key.
	 * Overwrite it to set specific rules.
	 *
	 * @param string $key
	 * @param mixed $value
	 * @param string|Exception &$error = null
	 * @return boolean
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Return result
		return true;
	}



	/**
	 * Check if property is removable,
     * from specified property key.
	 * Overwrite it to set specific rules.
	 *
	 * @param string $key
	 * @param string|Exception &$error = null
	 * @return boolean
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return true;
	}



	/**
	 * @inheritdoc
     * @throws HandleKeyInvalidFormatException
	 */
	protected function beanSetValidKey($key)
	{
	    // Call parent method
	    parent::beanSetValidKey($key);

		// Init var
		$error = null;

        // Check validation
        $tabCacheInfo = (static::$__beanTabCacheValidKey[$key] ?? null);
        if(is_null($tabCacheInfo))
        {
            $boolIsValid = $this->beanCheckValidKey($key, $error);
            static::$__beanTabCacheValidKey[$key] = array(
                $boolIsValid,
                $error
            );
        }
        else
        {
            $boolIsValid = $tabCacheInfo[0];
            $error = $tabCacheInfo[1];
        }

		// Throw exception, if required
		if(!$boolIsValid)
		{
			// Throw custom exception
			if($error instanceof Exception)
			{
				throw $error;
			}
			// Throw standard exception
			else
			{
				throw new HandleKeyInvalidFormatException($key, $error);
			}
		}
	}



	/**
	 * @inheritdoc
     * @throws HandleValueInvalidFormatException
	 */
	protected function beanSetValidValue($key, $value)
	{
        // Call parent method
        parent::beanSetValidValue($key, $value);

		// Init var
		$error = null;

        // Check validation, throw exception, if required
		if(!$this->beanCheckValidValue($key, $value, $error))
		{
			// Throw custom exception
			if($error instanceof Exception)
			{
				throw $error;
			}
			// Throw standard exception
			else
			{
				throw new HandleValueInvalidFormatException($key, $value, $error);
			}
		}
	}



	/**
	 * @inheritdoc
     * @throws HandleKeyNotRemoveException
	 */
	protected function beanSetValidRemove($key)
	{
        // Call parent method
        parent::beanSetValidRemove($key);

		// Init var
		$error = null;

        // Check validation
        $tabCacheInfo = (static::$__beanTabCacheValidRemove[$key] ?? null);
        if(is_null($tabCacheInfo))
        {
            $boolIsValid = $this->beanCheckValidRemove($key, $error);
            static::$__beanTabCacheValidRemove[$key] = array(
                $boolIsValid,
                $error
            );
        }
        else
        {
            $boolIsValid = $tabCacheInfo[0];
            $error = $tabCacheInfo[1];
        }

		// Throw exception, if required
		if(!$boolIsValid)
		{
			// Throw custom exception
			if($error instanceof Exception)
			{
				throw $error;
			}
			// Throw standard exception
			else
			{
				throw new HandleKeyNotRemoveException($key, $error);
			}
		}
	}
	
	


	
	// Methods events
	// ******************************************************************************
	
	/**
	 * Event before adding specified property.
	 * Overwrite it to set specific feature.
	 *
	 * @param string $key
	 * @param mixed $value
	 */
	protected function beanOnBeforeAddValue($key, $value)
	{
		// Do nothing
	}



	/**
	 * Event after adding specified property.
	 * Overwrite it to set specific feature.
	 *
	 * @param string $key
	 */
	protected function beanOnAfterAddValue($key)
	{
		// Do nothing
	}



	/**
	 * Event before setting specified property.
	 * Overwrite it to set specific feature.
	 *
	 * @param string $key
	 * @param mixed $value
	 */
	protected function beanOnBeforeSetValue($key, $value)
	{
		// Do nothing
	}



	/**
	 * Event after setting specified property.
	 * Overwrite it to set specific feature.
	 *
	 * @param string $key
	 */
	protected function beanOnAfterSetValue($key)
	{
		// Do nothing
	}



	/**
	 * Event before removing specified property.
	 * Overwrite it to set specific feature.
	 *
	 * @param string $key
	 */
	protected function beanOnBeforeRemoveValue($key)
	{
		// Do nothing
	}



	/**
	 * Event after removing specified property.
	 * Overwrite it to set specific feature.
	 *
	 * @param string $key
	 */
	protected function beanOnAfterRemoveValue($key)
	{
		// Do nothing
	}





	// Methods public interfaces
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanSetData($key, $value)
	{
		// Event before
		$this->beanOnBeforeSetValue($key, $value);

		// Call parent method
		parent::beanSetData($key, $value);

		// Event after
		$this->beanOnAfterSetValue($key);
	}



	/**
	 * @inheritdoc
	 */
	public function beanAddData($key, $value)
	{
		// Event before
		$this->beanOnBeforeAddValue($key, $value);

		// Call parent method
		parent::beanAddData($key, $value);

		// Event after
		$this->beanOnAfterAddValue($key);
	}



    /**
     * @inheritdoc
     */
    public function beanPutData($key, $value)
    {
        // Inti var
        $boolExists = $this->beanExists($key);

        // Event before
        if($boolExists)
        {
            $this->beanOnBeforeSetValue($key, $value);
        }
        else
        {
            $this->beanOnBeforeAddValue($key, $value);
        }

        // Call parent method
        parent::beanPutData($key, $value);

        // Event after
        if($boolExists)
        {
            $this->beanOnAfterSetValue($key);
        }
        else
        {
            $this->beanOnAfterAddValue($key);
        }
    }



	/**
	 * @inheritdoc
	 */
	public function beanRemoveData($key)
	{
		// Event before
		$this->beanOnBeforeRemoveValue($key);

		// Call parent method
		parent::beanRemoveData($key);

		// Event after
		$this->beanOnAfterRemoveValue($key);
	}



    /**
     * Remove bean cache.
     */
    public function beanRemoveCache()
    {
        parent::beanRemoveCache();

        static::$__beanTabCacheValidKey = array();
        static::$__beanTabCacheValidRemove = array();
    }



} 