<?php
/**
 * This class allows to define fixed bean class.
 * Fixed bean is default bean, allows to use only initialized properties.
 * Can be consider is base of all fixed bean types.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\bean\model;

use liberty_code\library\bean\model\DefaultBean;

use liberty_code\library\bean\exception\MethodForbiddenException;



abstract class FixBean extends DefaultBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods overwrite Bean public interfaces
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function beanAddData($key, $value)
	{
		throw new MethodForbiddenException(get_called_class(), __FUNCTION__);
	}



    /**
     * @inheritdoc
     */
    public function beanPutData($key, $value)
    {
        $this->beanSetData($key, $value);
    }



	/**
	 * @inheritdoc
	 */
	public function beanRemoveData($key)
	{
		throw new MethodForbiddenException(get_called_class(), __FUNCTION__);
	}
	
	
	
} 