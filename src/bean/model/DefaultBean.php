<?php
/**
 * Description :
 * This class allows to define default bean class.
 * Default bean is iterate bean, allows to initialize properties.
 * Can be consider is base of all default bean types.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\bean\model;

use liberty_code\library\bean\model\IterateBean;



abstract class DefaultBean extends IterateBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor / Others
	// ******************************************************************************

	/**
	 * @inheritdoc
     */
	public function __construct(array $tabData = array())
	{
		// Call parent constructor
		parent::__construct();

		// Hydrate
		$this->beanHydrateDefault();

        // Hydrate data
        $this->beanHydrate(
            $tabData,
            true,
            true,
            false
        );
	}
	
	
	
	
	
	// Methods initialize
	// ******************************************************************************
	
	/**
     * Hydrate properties,
     * with default values, at beginning.
	 * Overwrite it to set specific initialization.
	 */
	protected function beanHydrateDefault()
	{
		// Do nothing
	}
	
	
	
} 