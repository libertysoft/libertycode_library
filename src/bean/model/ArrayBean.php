<?php
/**
 * Description :
 * This class allows to define array bean class.
 * Array bean is handle bean, allows to use bean like properties array.
 * Properties can be accessible via property key/index.
 * Can be consider is base of all array bean types.
 *
 * Note:
 * -> Property index format:
 *     Must be a positif integer (0 to (Properties count - 1)).
 *     Each index corresponds to specific property key (Rank in properties data).
 * 
 * Feature:
 * -> Check property exists:
 *     - isset($this[integer property index])
 *     - isset($this['string property key'])
 *     => Equivalent to @see beanDataExists() .
 * -> Get property value:
 *     - $this[integer property index]
 *     - $this['string property key']
 *     - Equivalent to @see beanGetData() .
 * -> Set property:
 *     - $this[integer property index] = value
 *     - $this['string property key'] = value
 *     => Equivalent to @see beanSetData() .
 * -> Add property:
 *     - $this[integer property index] = value
 *     - $this['string property key'] = value
 *     => Equivalent to @see beanAddData() .
 * -> Put property (set if exists, add else):
 *     - $this[integer property index] = value
 *     - $this['string property key'] = value
 *     => Equivalent to @see beanPutData() .
 * -> Remove property:
 *     - unset($this[integer property index])
 *     - unset($this['string property key'])
 *     => Equivalent to @see beanRemoveData() .
 * -> Run all properties:
 *     Use Count($this) to loop property index.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\bean\model;

use Countable;
use ArrayAccess;
use liberty_code\library\bean\model\HandleBean;

use liberty_code\library\bean\exception\IndexInvalidFormatException;
use liberty_code\library\bean\exception\IndexNotFoundException;
use liberty_code\library\bean\exception\KeyIndexInvalidFormatException;



abstract class ArrayBean extends HandleBean implements Countable, ArrayAccess
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get property key,
     * from specified property index.
     *
     * @param integer $intIndex
     * @param boolean $boolThrowRequired = false
     * @return null|string
     * @throws IndexInvalidFormatException
     * @throws IndexNotFoundException
     */
    public function beanGetStrKeyFromIndex(
        $intIndex,
        $boolThrowRequired = false
    )
    {
        // Set check format index
        IndexInvalidFormatException::setCheck($intIndex);

        // Init var
        $boolThrowRequired = (is_bool($boolThrowRequired) ? $boolThrowRequired : false);
        $tabKey = array_keys($this->__beanTabData);
        $result = ($tabKey[$intIndex] ?? null);

        // Check valid index, if required
        if($boolThrowRequired && is_null($result))
        {
            throw new IndexNotFoundException($intIndex);
        }

        // Return result
        return $result;
    }



    /**
     * Get property key,
     * from specified key (property index|property key).
     *
     * @param integer|string $key
     * @param boolean $boolThrowRequired = false
     * @return null|string
     * @throw KeyIndexInvalidFormatException
     */
    protected function beanGetStrKey($key, $boolThrowRequired = false)
    {
        // Set check format key
        KeyIndexInvalidFormatException::setCheck($key);

        // Return result
        return (
            is_int($key) ?
                $this->beanGetStrKeyFromIndex($key, $boolThrowRequired) :
                $key
        );
    }




	
	// Methods Countable
	// ******************************************************************************
	
	/**
	 * @inheritdoc
     */
	public function count()
	{
		return count($this->__beanTabData);
	}
	
	
	
	
	
	// Methods ArrayAccess
	// ******************************************************************************
	
	/**
     * @inheritdoc
	 * @param integer|string $key
	 */
	public function offsetExists($key)
	{
        return (!is_null($this->beanGetStrKey($key)));
	}
	
	
	
	/**
     * @inheritdoc
	 * @param integer|string $key
	 */
	public function offsetGet($key)
	{
	    // Init var
        $strKey = $this->beanGetStrKey($key, true);

        // Get property
		return $this->beanGetData($strKey);
	}
	
	
	
	/**
     * @inheritdoc
	 * @param integer|string $key
	 */
	public function offsetSet($key, $value)
	{
        // Init var
        $strKey = $this->beanGetStrKey($key, true);

        // Set property
        $this->beanPutData($strKey, $value);
	}
	
	
	
	/**
     * @inheritdoc
	 * @param integer|string $key
	 */
	public function offsetUnset($key)
	{
        // Init var
        $strKey = $this->beanGetStrKey($key, true);

        // Remove property
		$this->beanRemoveData($strKey);
	}
	
	
	
} 