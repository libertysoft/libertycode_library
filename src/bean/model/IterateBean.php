<?php
/**
 * Description :
 * This class allows to define iterate bean class.
 * Iterate bean is array bean, allows to use bean like properties iterator (ex: in foreach loop).
 * Can be consider is base of all iterate bean types.
 *
 * Feature:
 * -> Run all properties:
 *     Use foreach($this as $propertyKey => $propertyValue).
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\library\bean\model;

use Iterator;
use liberty_code\library\bean\model\ArrayBean;



abstract class IterateBean extends ArrayBean implements Iterator
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	/**
	 * Position of actual cursor in the iteration.
     * @var int
     */
	protected $__beanIntCursor;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor / Others
	// ******************************************************************************

	/**
	 * @inheritdoc
     */
	public function __construct(array $tabData = array())
	{
		// Call parent constructor
		parent::__construct($tabData);
		
		// Init var
		$this->__beanIntCursor = 0;
	}
	
	
	
	/**
	 * @inheritdoc
     */
	public function __clone()
    {
		// Call parent clone
		parent::__clone();
		
		// Init var
		$this->__beanIntCursor = 0;
    }
	
	
	
	
	
	// Methods Iterator
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function key()
	{
		return $this->beanGetStrKeyFromIndex($this->__beanIntCursor, true);
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function current()
	{
		return $this->beanGetData($this->key());
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function next()
	{
		$this->__beanIntCursor++;
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function rewind()
	{
		$this->__beanIntCursor = 0;
	}
	
	
	
	/**
	* @inheritdoc
	*/
	public function valid()
	{
		return (($this->__beanIntCursor >= 0) && ($this->__beanIntCursor < $this->count()));
	}
	
	
	
} 