<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/instance/library/ConstInstance.php');
include($strRootPath . '/src/instance/exception/IndexInvalidFormatException.php');
include($strRootPath . '/src/instance/exception/IndexNotFoundException.php');
include($strRootPath . '/src/instance/exception/InstanceNotFoundException.php');
include($strRootPath . '/src/instance/exception/InstanceNewForbiddenException.php');
include($strRootPath . '/src/instance/exception/OptionInvalidFormatException.php');
include($strRootPath . '/src/instance/model/Instance.php');
include($strRootPath . '/src/instance/model/Multiton.php');
include($strRootPath . '/src/instance/model/Singleton.php');

include($strRootPath . '/src/bean/library/ConstBean.php');
include($strRootPath . '/src/bean/exception/KeyInvalidFormatException.php');
include($strRootPath . '/src/bean/exception/KeyFoundException.php');
include($strRootPath . '/src/bean/exception/KeyNotFoundException.php');
include($strRootPath . '/src/bean/exception/IndexInvalidFormatException.php');
include($strRootPath . '/src/bean/exception/IndexNotFoundException.php');
include($strRootPath . '/src/bean/exception/KeyIndexInvalidFormatException.php');
include($strRootPath . '/src/bean/exception/OptionInvalidFormatException.php');
include($strRootPath . '/src/bean/exception/MethodForbiddenException.php');
include($strRootPath . '/src/bean/exception/HandleKeyInvalidFormatException.php');
include($strRootPath . '/src/bean/exception/HandleValueInvalidFormatException.php');
include($strRootPath . '/src/bean/exception/HandleKeyNotRemoveException.php');
include($strRootPath . '/src/bean/model/Bean.php');
include($strRootPath . '/src/bean/model/HandleBean.php');
include($strRootPath . '/src/bean/model/ArrayBean.php');
include($strRootPath . '/src/bean/model/IterateBean.php');
include($strRootPath . '/src/bean/model/DefaultBean.php');
include($strRootPath . '/src/bean/model/FixBean.php');

include($strRootPath . '/src/datetime/library/ConstDateTime.php');
include($strRootPath . '/src/datetime/library/ToolBoxDateTime.php');
include($strRootPath . '/src/datetime/exception/TzDefaultInvalidFormatException.php');
include($strRootPath . '/src/datetime/exception/FormatDbInvalidFormatException.php');
include($strRootPath . '/src/datetime/exception/FormatListInvalidFormatException.php');
include($strRootPath . '/src/datetime/model/ConfigDateTime.php');

include($strRootPath . '/src/table/library/ConstTable.php');
include($strRootPath . '/src/table/library/ToolBoxTable.php');
include($strRootPath . '/src/table/exception/MergeOptionInvalidFormatException.php');
include($strRootPath . '/src/table/exception/MergeOptionStopException.php');

include($strRootPath . '/src/reflection/library/ToolBoxReflection.php');
include($strRootPath . '/src/reflection/library/ToolBoxClassReflection.php');
include($strRootPath . '/src/reflection/library/ToolBoxFileReflection.php');

include($strRootPath . '/src/error/library/ToolBoxError.php');

include($strRootPath . '/src/regexp/library/ToolBoxRegexp.php');

include($strRootPath . '/src/file/library/ToolBoxFile.php');

include($strRootPath . '/src/random/library/ToolBoxRandom.php');

include($strRootPath . '/src/str/library/ToolBoxString.php');

include($strRootPath . '/src/crypto/library/ToolBoxHash.php');