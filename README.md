LibertyCode_Library
===================



Description
-----------

Library contains basic components, 
- to build components.
- to use basic features.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/library ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/library": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Configuration
-------------

#### Datetime

- Use following class to configure specific elements
    
    ```php
    use liberty_code\library\datetime\library\ToolBoxDateTime;
    ToolBoxDateTime::getObjConfig()->get|set...();
    
    // or
    
    use liberty_code\library\datetime\model\ConfigDateTime;
    ConfigDateTime::instanceGetDefault()->get|set...();
    ```

- Elements configurables

    - Datetime default timezone
    - Datetime string format for database
    - List of datetime string default formats

---



Usage
-----

#### Instance

Instance allows to handle class instances.

_Elements_

- Instance
    
    Allows to handle its owns class instances.

- Multiton

    Extends instance features. 
    Allows to handle class instantiation control.

_Example_

```php
// Define class
class Test extends liberty_code\library\instance\model\Multiton
{
    ...
}
...
// Get class iinstance
$object = Test::instanceGetDefault();
...
```

#### Bean

Bean allows to handle properties.

_Elements_

- Bean
    
    Extends multiton features. 
    Allows to handle properties.

- HandleBean

    Extends bean features. 
    Allows to handle property control.

- ArrayBean

    Extends handle bean features. 
    Allows to use bean like properties array.

- IterateBean

    Extends array bean features. 
    Allows to use bean like properties iterator, 
    to easily reach each property, in loop.

- DefaultBean

    Extends iterate bean features. 
    Allows to initialize properties.
    Usefull to design collection.

- FixBean

    Extends default bean features. 
    Allows to use only initialized properties.
    Usefull to design model.

_Example_

```php
// Define model class
class Test extends liberty_code\library\bean\model\FixBean
{
    public function beanHydrateDefault()
    {
        $this->beanHydrate(
            array(
                'item1' => 'value 1', 
                'item2' => 2
            )
        );
    }
}
...
// Get model instance
$object = new Test();
...
// Get model properties values
echo($object->getItem1()); // Show string "value 1"
echo($object->getItem2()); // Show integer 2
...
// Set model properties values
echo($object['item1']); // Show string "value 1"
echo($object['item2']); // Show integer 2
...
foreach($object as $key => $value) {
    echo($key . ' => ' . $value .'<br />');
}
/**
 * Show: 
 * item1 => value 1
 * item2 => 2
 */
...
```

#### Utility

- Datetime

    Date, time and timezone features, with configuration.
    
- Table 
    
    Array features.
    
- Reflection

    Reflection features.  
    
- Error
    
    Error features.
    
- Regexp
    
    Regular expression features.
    
- File

    File features.

- Random

    Random features.
    
- String 
    
    String features.
    
- Crypto

    Cryptography and hashing features.
     
---


